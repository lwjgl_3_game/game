package fr.game.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.fcgame.client.components.World;
import fr.fcgame.client.utils.java.Pair;

public class TestIndex {

	@Test
	public void testIndex1() {
		assertEquals(0, indexInArray(-16, 0, -16));
	}
	
	@Test
	public void testIndex2() {
		assertEquals(64, indexInArray(-16, 1, -16));
	}
	
	@Test
	public void testIndex3() {
		assertEquals(128, indexInArray(-16, 2, -16));
	}
	
	@Test
	public void testIndex4() {
		assertEquals(278, indexInArray(14, 12, -6, true));
	}
	
	@Test
	public void testIndex5() {
		assertEquals(342, indexInArray(14, 13, -6, true));
	}
	
	private int indexInArray(int x, int y, int z) {
		return indexInArray(x, y, z, false);
	}
	
	private int indexInArray(int x, int y, int z, boolean b) {
		Pair<Integer, Integer> c = World.getChunkXYAt(x, z); 
		int exactX = x < 0 ? (World.CHUNK_SIZE * -c.getT1() + x) : x;
		int exactZ = z < 0 ? (World.CHUNK_SIZE * -c.getT2() + z) : z;
		if(b) {
			System.out.println(x+" x > "+exactX);
			System.out.println(z+" z > "+exactZ);
		}
		
		int index = (y%World.CHUNK_HEIGHT) * World.CHUNK_SIZE * World.CHUNK_SIZE
				 + (exactZ%World.CHUNK_SIZE) * World.CHUNK_SIZE
				 + (exactX%World.CHUNK_SIZE);
		return index;
	}
	
	
}
