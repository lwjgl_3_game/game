#version 400 core

in vec2 pass_textureCoords;
in vec3 toLightVector;
in float outLight;

out vec4 out_Colour;

uniform sampler2D textureSampler;
uniform vec3 lightColour;

void main(void){

	// Light calc
	vec4 texture = texture(textureSampler, pass_textureCoords);
	if(texture.a < 0.5){
		discard;
	}
	out_Colour = vec4(outLight, outLight, outLight, 1.0) * texture;

}
