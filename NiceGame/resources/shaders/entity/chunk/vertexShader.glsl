#version 400 core

in vec3 position;
in vec2 textureCoords;
in float light;

out vec2 pass_textureCoords;
out vec3 toLightVector;
out float outLight;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec3 lightPosition;

void main(void){

	vec4 worldPosition = transformationMatrix * vec4(position.xyz, 1.0);
	gl_Position = projectionMatrix * viewMatrix * worldPosition;
	pass_textureCoords = textureCoords;

	outLight = light;
	toLightVector = lightPosition - worldPosition.xyz;
}
