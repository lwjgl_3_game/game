#version 400 core

in vec2 textureCoords;
in vec3 surfaceNormal;
in vec3 toLightV;

out vec4 out_Colour;

uniform sampler2D textureSampler;
uniform vec3 lightColour;

void main(void){

	// Light calc
	vec3 unitNormal = normalize(surfaceNormal);
	vec3 unitLight = normalize(toLightV);

	float nDot = dot(unitNormal, unitLight);
	float brightness = max(nDot, 0.2);
	vec3 diffuse = brightness * lightColour;

	out_Colour = vec4(diffuse, 1.0) * texture(textureSampler, textureCoords);

}
