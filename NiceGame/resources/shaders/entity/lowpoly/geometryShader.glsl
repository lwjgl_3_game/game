#version 400 core

layout ( triangles ) in;
layout ( triangle_strip, max_vertices = 3 ) out;

in vec2[] pass_textureCoords;
in vec3[] toLightVector;

out vec2 textureCoords;
out vec3 surfaceNormal;
out vec3 toLightV;

vec3 calculateTriangleNormal(){
	vec3 tangent = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
	vec3 bitangent = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;
	vec3 normal = cross(tangent, bitangent);
	return normalize(normal);
}

void main(void){
	//Operation i
	for(int i = 0; i < 3; i++){
		gl_Position = gl_in[i].gl_Position;
		textureCoords = pass_textureCoords[i];
		surfaceNormal = calculateTriangleNormal();
		toLightV = toLightVector[i];
		EmitVertex();
	}
	EndPrimitive();

}
