#version 400 core

in vec3 position;
in vec2 textureCoords;
in float light;

out vec2 pass_textureCoords;
out vec3 toLightVector;
out float outLight;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec3 lightPosition;
uniform float time;

const float PI = 3.1415926535897932384626433832795;


void main(void){

	vec4 worldPosition = transformationMatrix * vec4(position.xyz, 1.0);
	gl_Position = projectionMatrix * viewMatrix * worldPosition;
	pass_textureCoords = textureCoords;

	outLight = light;
	toLightVector = lightPosition - worldPosition.xyz;
}
