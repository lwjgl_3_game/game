#version 400 core

out vec4 out_Colour;

uniform vec3 colour;

void main(void){

	out_Colour = vec4(colour, 1.0);

}
