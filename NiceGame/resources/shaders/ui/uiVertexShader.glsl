#version 400 core

in vec3 position;

uniform mat4 projectionMatrix;

void main(void){

	gl_Position = projectionMatrix * vec4(position, 1.0);

}
