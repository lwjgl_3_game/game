package fr.fcgame.client.meshes;

import fr.fcgame.client.meshes.generators.ChunkMeshGenerator;
import fr.fcgame.client.meshes.generators.FlowerMeshGenerator;

public enum ChunkMeshType {

	RAW(ChunkMeshGenerator.class),
	FLOWER(FlowerMeshGenerator.class);
	
	private Class<? extends WorldComponentMeshGenerator> meshGenerator;
	
	private ChunkMeshType(Class<? extends WorldComponentMeshGenerator> meshGenerator) {
		this.meshGenerator = meshGenerator;
	}
	
	public Class<? extends WorldComponentMeshGenerator> getMeshGenerator() {
		return meshGenerator;
	}
	
}
