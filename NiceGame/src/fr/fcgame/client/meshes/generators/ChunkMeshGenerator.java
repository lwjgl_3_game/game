package fr.fcgame.client.meshes.generators;

import fr.fcgame.client.Game;
import fr.fcgame.client.components.Chunk;
import fr.fcgame.client.components.Face;
import fr.fcgame.client.components.blocks.Block;
import fr.fcgame.client.components.material.Material;
import fr.fcgame.client.meshes.WorldComponentMeshGenerator;

public class ChunkMeshGenerator extends WorldComponentMeshGenerator{

	public static final float[] FRONT_FACE = {
			  //x, y, z
				1, 1, 1,
				0, 1, 1,
				0, 0, 1,
				1, 0, 1};

	public static final float[] BACK_FACE = {
			0, 1, 0,
			1, 1, 0,
			1, 0, 0,
			0, 0, 0};

	public static final float[] LEFT_FACE = {
		  	0, 1, 1,
		  	0, 1, 0,
		  	0, 0, 0,
		  	0, 0, 1};
	

	public static final float[] RIGHT_FACE = {
			1, 1, 0,
			1, 1, 1,
			1, 0, 1,
			1, 0, 0};
	

	public static final float[] TOP_FACE = {
			1, 1, 0,
			0, 1, 0,
			0, 1, 1,
			1, 1, 1};


	public static final float[] BOTTOM_FACE = {
			0, 0, 0,
			1, 0, 0,
			1, 0, 1,
			0, 0, 1};

	public ChunkMeshGenerator(Chunk chunk) {
		super(chunk);
	}

	@Override
	protected void doMesh(int x, int y, int z, Block block, Material type) {
		if(!type.isFlower()) {
			// top
			if(!Game.getGame().getWorld().getBlockTypeAt(x, y+1, z).isOpaque()) {
				addMeshFace(TOP_FACE, x, y, z, type.getUVCoords().get(Face.TOP), 1.0f);
			}
			// bot
			if(!Game.getGame().getWorld().getBlockTypeAt(x, y-1, z).isOpaque()) {
				addMeshFace(BOTTOM_FACE, x, y, z, type.getUVCoords().get(Face.BOTTOM), 0.2f);
			}
			// left
			if(!Game.getGame().getWorld().getBlockTypeAt(x+1, y, z).isOpaque()) {
				addMeshFace(RIGHT_FACE, x, y, z, type.getUVCoords().get(Face.RIGHT), 0.4f);
			}
			// right
			if(!Game.getGame().getWorld().getBlockTypeAt(x-1, y, z).isOpaque()) {
				addMeshFace(LEFT_FACE, x, y, z, type.getUVCoords().get(Face.LEFT), 0.4f);
			}
			// front
			if(!Game.getGame().getWorld().getBlockTypeAt(x, y, z-1).isOpaque()) {
				addMeshFace(BACK_FACE, x, y, z, type.getUVCoords().get(Face.BACK), 0.4f);
			}
			// back
			if(!Game.getGame().getWorld().getBlockTypeAt(x, y, z+1).isOpaque()) {
				addMeshFace(FRONT_FACE, x, y, z, type.getUVCoords().get(Face.FRONT), 0.4f);
			}
		}
	}
}
