package fr.fcgame.client.meshes.generators;

import fr.fcgame.client.components.Chunk;
import fr.fcgame.client.components.Face;
import fr.fcgame.client.components.blocks.Block;
import fr.fcgame.client.components.material.Material;
import fr.fcgame.client.meshes.WorldComponentMeshGenerator;

public class FlowerMeshGenerator extends WorldComponentMeshGenerator{

	public static final float[] SIDE_FACE_A = {
			1, 1, 0,
		  	0, 1, 1,
		  	0, 0, 1,
		  	1, 0, 0};

	private static final float[] SIDE_FACE_B = {
			0, 1, 0,
		  	1, 1, 1,
		  	1, 0, 1,
		  	0, 0, 0};
	
	public FlowerMeshGenerator(Chunk chunk) {
		super(chunk);
	}

	@Override
	protected void doMesh(int x, int y, int z, Block block, Material type) {
		if(type.isFlower()) {
			addMeshFace(SIDE_FACE_A, x, y, z, type.getUVCoords().get(Face.TOP), 0.7f);
			addMeshFace(SIDE_FACE_B, x, y, z, type.getUVCoords().get(Face.TOP), 0.7f);
		}
	}
	
}
