package fr.fcgame.client.meshes;

import org.joml.Vector3f;

import fr.fcgame.client.utils.lwjgl.model.Mesh;
import fr.fcgame.client.utils.lwjgl.object.Meshable;

/**
 * 
 * @author ComminQ_Q (Computer)
 *
 * @date 02/12/2019
 */
public abstract class EntityModel implements Meshable{

	private int entityID; // ID de l'entité
	private Mesh mesh; // Mesh complet (texture, vertices, animation)
	private Vector3f position; // Position dans le monde
	
	/**
	 * Constructeur
	 * @author ComminQ_Q (Computer)
	 *
	 * @date 02/12/2019
	 */
	public EntityModel() {
		this.entityID = -1;
		this.position = new Vector3f();
	}
	
	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @return Si cette entité existe ou pas
	 * @date 02/12/2019
	 */
	public boolean isSet() {
		return this.entityID != -1;
	}
	
	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @return Renvoie l'ID de l'entité
	 * @date 02/12/2019
	 */
	public int getEntityID() {
		return entityID;
	}
	
	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @return Le Mesh
	 * @date 02/12/2019
	 */
	@Override
	public Mesh getMesh() {
		return this.mesh;
	}
	
	public void setMesh(Mesh mesh) {
		this.mesh = mesh;
	}
	
	public void translate(float dx, float dy, float dz) {
		this.position.x += dx;
		this.position.y += dy;
		this.position.z += dz;
	}
	
	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param entityID L'ID de l'entité
	 * @date 02/12/2019
	 */
	public void setEntityID(int entityID) {
		if(!isSet()) {
			this.entityID = entityID;
		}
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @return La position de l'entité dans le monde
	 * @date 23/12/2019
	 */
	public Vector3f getPosition() {
		return position;
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param position
	 * @date 02/12/2019
	 */
	public void setPosition(Vector3f position) {
		this.position = position;
	}
	
}
