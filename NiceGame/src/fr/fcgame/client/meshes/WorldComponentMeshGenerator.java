package fr.fcgame.client.meshes;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import fr.fcgame.client.components.Chunk;
import fr.fcgame.client.components.blocks.Block;
import fr.fcgame.client.components.material.Material;
import fr.fcgame.client.utils.lwjgl.model.IndicesMesh;
import fr.fcgame.client.utils.vao.Vao;

public abstract class WorldComponentMeshGenerator {

	private Chunk chunk;
	private int indicesCount = 0;
	private List<Float> vertices;
	private List<Integer> indices;
	private List<Float> textureCoords;
	private List<Float> normals;
	private List<Float> lightPower;
	
	public WorldComponentMeshGenerator(Chunk chunk) {
		this.chunk = chunk;
		this.vertices = new ArrayList<>();
		this.indices = new ArrayList<>();
		this.textureCoords = new ArrayList<>();
		this.normals = new ArrayList<>();
		this.lightPower = new ArrayList<>();
	}
	
	protected abstract void doMesh(int x, int y, int z, Block block, Material type);
	
	private IndicesMesh createVAOForChunk(int[] indices,
								   float[] positions,
								   float[] textureCoords,
								   float[] lightPower) {
		// Init VAO
		Vao vao = Vao.createVao();
		// Pour manipuler une VAO, on doit l'activer
		vao.bind();
		// On ajoute les indices des points (Optimisations)
		vao.bindIndices(indices);

		// On ajoute les positions des points
		vao.bindData(positions, 3); // Positions (3D)
		vao.bindData(textureCoords, 2); // Textures (2D)
		vao.bindData(lightPower, 1); // Lumière / blocks (1D)
		// On désactive la VAO
		vao.unbind();
		IndicesMesh indicesMesh = new IndicesMesh(vao, indices.length);
		return indicesMesh;
	}
	
	public IndicesMesh createMesh() {
		this.indicesCount = 0;
		this.vertices.clear();
		this.textureCoords.clear();
		this.indices.clear();
		this.normals.clear();
		
		for(Block block : this.chunk.getBlocks()) {
			int y = block.getBlockPosition().y;
			int x = block.getBlockPosition().x;
			int z = block.getBlockPosition().z;
			Material type = block.getType();
			if(type != Material.AIR) {
				doMesh(x, y, z, block, type);
			}
		}

		float[] vertices = new float[this.vertices.size()];
		int[] indices = new int[this.indices.size()];
		float[] textureCoords = new float[this.textureCoords.size()];
		float[] normals = new float[this.normals.size()];
		float[] lights = new float[this.lightPower.size()];
		int i = 0;
		for(i = 0; i < this.vertices.size(); i++)vertices[i] = this.vertices.get(i);
		for(i = 0; i < this.indices.size(); i++)indices[i] = this.indices.get(i);
		for(i = 0; i < this.textureCoords.size(); i++)textureCoords[i] = this.textureCoords.get(i);
		for(i = 0; i < this.normals.size(); i++)normals[i] = this.normals.get(i);
		for(i = 0; i < this.lightPower.size(); i++)lights[i] = this.lightPower.get(i);
		
		return createVAOForChunk(indices, vertices, textureCoords, lights);
	}
	
	protected void addMeshFace(float[] meshFace, int x, int y, int z, float[] texturesCoords, float lightPower) {
		int index = 0;
		for(int i = 0; i < 4; i++) {
			this.vertices.add(meshFace[index++] + x);
			this.vertices.add(meshFace[index++] + y);
			this.vertices.add(meshFace[index++] + z);
			this.lightPower.add(lightPower);
		}
		for(float uv : texturesCoords) {
			this.textureCoords.add(uv);
		}
		
		this.indices.add(this.indicesCount);
		this.indices.add(this.indicesCount+1);
		this.indices.add(this.indicesCount+2);
		this.indices.add(this.indicesCount+2);
		this.indices.add(this.indicesCount+3);
		this.indices.add(this.indicesCount);
		
		this.indicesCount+=4;
	}
	
}
