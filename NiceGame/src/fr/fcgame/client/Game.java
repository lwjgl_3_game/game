
package fr.fcgame.client;

import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwTerminate;

import java.awt.Color;

import fr.fcgame.client.components.Chunk;
import fr.fcgame.client.components.World;
import fr.fcgame.client.components.material.Material;
import fr.fcgame.client.entity.EntityPlayer;
import fr.fcgame.client.events.EventsManager;
import fr.fcgame.client.listeners.BlockInteractionListener;
import fr.fcgame.client.renderer.MainRenderer;
import fr.fcgame.client.renderer.renderers.ChunkCompoundRenderer;
import fr.fcgame.client.renderer.renderers.ChunkRenderer;
import fr.fcgame.client.renderer.renderers.FlowerRenderer;
import fr.fcgame.client.renderer.renderers.SkyboxRenderer;
import fr.fcgame.client.renderer.shader.defaut.ChunkShader;
import fr.fcgame.client.renderer.shader.defaut.DefaultShader;
import fr.fcgame.client.renderer.shader.defaut.FlowerShader;
import fr.fcgame.client.renderer.shader.defaut.SkyboxShader;
import fr.fcgame.client.state.Controller;
import fr.fcgame.client.utils.java.Cooldown;
import fr.fcgame.client.utils.lwjgl.Camera;
import fr.fcgame.client.utils.lwjgl.Helper;
import fr.fcgame.client.utils.lwjgl.Renderer;
import fr.fcgame.client.utils.lwjgl.Window;
import fr.fcgame.client.utils.lwjgl.texture.CubeMapTexture;
import fr.fcgame.client.utils.lwjgl.texture.Texture;
import fr.fcgame.client.utils.ui.UiComponent;
import fr.fcgame.client.utils.ui.UiRenderer;
import fr.fcgame.client.utils.ui.shader.UiShader;
import fr.fcgame.client.utils.vao.Vao;
import fr.fcgame.client.utils.vao.Vbo;

/**
 * Classe représentant un jeu, Singleton Patern
 * @author fabien
 * @date 07/12/2019
 *
 */
public class Game implements Runnable{

	private static Game game; // Singleton Game
	public static final int i = 0;
	
	/**
	 * 
	 * @author fabien
	 * @date 07/12/2019
	 * @return L'instance Game, ou créer l'instance
	 */
	public static synchronized Game getGame() {
		if(game == null) {
			game = new Game();
		}
		return game;
	}
	
	public static int WIDTH = 1280; // La largeur
	public static int HEIGHT = 720; // La hauteur
	
	private final Thread mainThread;
	private final Window window;
	private final MainRenderer mainRenderer;
	private Camera camera;
	private World world;
	private Controller currentController;
	private ChunkRenderer chunkRender;
	private EventsManager eventsManager;
	
	private double lastMouseInteraction = 0;
	private double lastFrame;
	
	/**
	 * Constructeur
	 * @author fabien
	 * @date 07/12/2019
	 */
	private Game() {
		this.mainThread = Thread.currentThread();
		this.window = Window.initWindow("Le jeu", 1280, 720);
		this.mainRenderer = new MainRenderer();
		
		this.window.center();
		this.window.setClearColor(new Color(255, 0, 0));
		
		// Active ou non la VSync
		this.window.setVSync(false);
		Material.initAll();
		this.world = new World(this);
		this.currentController = Controller.GAME;
		this.eventsManager = new EventsManager();
	}

	private void init() {
		// On initialise tous les components
		EntityPlayer entityPlayer = new EntityPlayer();
		this.camera = new Camera(this, entityPlayer);
		UiComponent.init(this.window, this);
		
		this.world.getEntities().add(entityPlayer);

		this.mainRenderer.registerShader(new DefaultShader());
		this.mainRenderer.registerShader(new ChunkShader());
		FlowerShader flowerShader = new FlowerShader();
		this.mainRenderer.registerShader(flowerShader);
		
		// Le Shader qui s'occupe du GUI
		UiShader uiShader = new UiShader();
		// Matrice de projection 2D pour le GUI
		uiShader.buildProjectionMatrix();
		
		this.mainRenderer.registerShader(uiShader);
		SkyboxShader skyboxShader = new SkyboxShader();
		this.mainRenderer.registerShader(skyboxShader);
		
		SkyboxRenderer skyboxRenderer = new SkyboxRenderer(this, skyboxShader);
		
		this.chunkRender = new ChunkRenderer();
		FlowerRenderer flowerRenderer = new FlowerRenderer(flowerShader);
		this.mainRenderer.add(flowerRenderer);
		this.mainRenderer.add(this.chunkRender);
		this.mainRenderer.add(skyboxRenderer);
		this.mainRenderer.add(new UiRenderer());
		this.window.hideCursor();
		
		// End init
		this.mainRenderer.init(this, this.window);
		this.mainRenderer.setWireframe(false);
		
		this.world.initChunkModels(this.chunkRender);
		this.world.getEntities().add(entityPlayer);
		
		this.eventsManager.registerListener(new BlockInteractionListener());
	}
	
	/**
	 * 
	 * @author fabien
	 * @date 07/12/2019
	 * @return le Thread principal dans lequel le jeu a été lancé
	 */
	public Thread getMainThread() {
		return mainThread;
	}
	
	@Override
	public void run() {
		// On active l'initialisation
		this.init();
		this.lastFrame = Helper.getTime();
		while(!this.window.hasToBeClosed()) {
			// On récup tous les events qui ont été effectué
			this.window.pollEvents();
			
			this.readInput();
			this.world();
			
			this.window.setWindowName(this.window.getMainMonitor().getName()+" | FPS = "+this.window.getFps());
			// Ici on s'occupe du rendu des trucs
			// Style GUI, Objet, Terrain, Entité
			this.mainRenderer.renderScene(null);
			
			// On échange le buffer avant et le buffer arrière 
			this.window.swapBuffers();
		}
		this.end();
	}
	
	/**
	 * 
	 * @author fabien
	 * @date 31/01/2020
	 * @return La différence de temps entre le temps actuel et le temps de la dernière image générée
	 */
	public double getDelta() {
		return this.window.getDelta();
	}
	
	private void world() {
		if(!Cooldown.exist("world")) {
			Cooldown.create("world", 20L);
		}
		if(Cooldown.elapsed("world")) {
			this.camera.processPlayer();
			Cooldown.reset("world");
		}
	}
	
	/**
	 * Lit les entrées souris/clavier
	 * @author fabien
	 * @date 31/01/2020
	 */
	public void readInput() {
		this.camera.moveMouse();
		
		this.camera.move();
		this.world.updateFrustumsPlanes(this.camera);
		
		if(this.lastMouseInteraction + 50 < Helper.getTime()) {
			this.lastMouseInteraction = Helper.getTime();
			this.camera.mouse();
		}
		
	}
	
	/**
	 * Termine et ferme la fenêtre
	 * @author fabien
	 * @date 07/12/2019
	 */
	public void end() {
		// Free the window callbacks and destroy the window
		this.window.close();

		Vao.unbindAll();
		Vbo.unbindAll();
		Texture.clear();
		CubeMapTexture.clear();
		this.mainRenderer.clean();
		
		// Terminate GLFW and free the error callback
		glfwTerminate();
		glfwSetErrorCallback(null).free();
		System.gc();
	}

	
	public World getWorld() {
		return world;
	}
	
	public MainRenderer getMainRenderer() {
		return mainRenderer;
	}
	
	public Camera getCamera() {
		return camera;
	}

	public Controller getCurrentController() {
		return currentController;
	}

	public void setCurrentController(Controller currentController) {
		this.currentController = currentController;
	}

	public ChunkRenderer getChunkRender() {
		return chunkRender;
	}

	public EventsManager getEventsManager() {
		return eventsManager;
	}

	public void updateChunk(Chunk chunk) {
		for(Renderer<?> renderer : this.mainRenderer.getRenderers()) {
			if(renderer instanceof ChunkCompoundRenderer) {
				ChunkCompoundRenderer chunkRenderer = (ChunkCompoundRenderer) renderer;
				chunkRenderer.updateChunk(chunk);
			}
		}
	}
	
}
