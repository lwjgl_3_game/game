package fr.fcgame.client.listeners;

import fr.fcgame.client.components.blocks.Block;
import fr.fcgame.client.components.material.Material;
import fr.fcgame.client.events.EventMethod;
import fr.fcgame.client.events.Listener;
import fr.fcgame.client.events.world.block.BlockBreakEvent;

public class BlockInteractionListener implements Listener{

	@EventMethod
	public void onBlockBreak(BlockBreakEvent event) {
		Block getUpBlock = event.getBlock().getBlockAbove();
		if(getUpBlock.getType().isFlower()) {
			getUpBlock.setType(Material.AIR);
		}
	}
	
}
