package fr.fcgame.client.events.world.block;

import fr.fcgame.client.components.blocks.Block;
import fr.fcgame.client.events.Cancellable;

public class BlockBreakEvent extends BlockEvent implements Cancellable{

	private boolean cancel;
	
	public BlockBreakEvent(Block block) {
		super(block);
		this.cancel = false;
	}

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancel = cancelled;
	}

}
