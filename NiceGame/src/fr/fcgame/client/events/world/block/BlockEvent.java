package fr.fcgame.client.events.world.block;

import fr.fcgame.client.components.blocks.Block;
import fr.fcgame.client.events.Event;

public abstract class BlockEvent extends Event{

	private Block block;
	
	public BlockEvent(Block block) {
		this.block = block;
	}
	
	public Block getBlock() {
		return block;
	}
	
}
