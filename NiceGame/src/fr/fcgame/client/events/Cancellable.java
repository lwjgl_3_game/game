package fr.fcgame.client.events;

public interface Cancellable {

	public boolean isCancelled();
	
	public void setCancelled(boolean cancelled);
	
}
