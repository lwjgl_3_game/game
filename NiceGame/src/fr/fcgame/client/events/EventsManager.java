package fr.fcgame.client.events;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

import fr.fcgame.client.Game;

public class EventsManager {
	
	public static void callEvent(Event event) {
		try {
			Game.getGame().getEventsManager().invokeEvent(event);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private List<Listener> listeners;
	
	public EventsManager() {
		this.listeners = new ArrayList<>();
	}
	
	public void registerListener(Listener listener) {
		this.listeners.add(listener);
	}

	public <T extends Event> void invokeEvent(T event) throws IllegalAccessException,
															IllegalArgumentException,
															InvocationTargetException {
		for(Listener listener : this.listeners) {
			Method[] methods = listener.getClass().getDeclaredMethods();
			for(Method method : methods) {
				if(method.isAnnotationPresent(EventMethod.class)) {
					if(method.getParameterCount() == 1) {
						Class<?> clazz = method.getParameterTypes()[0];
						if(clazz.isAssignableFrom(event.getClass())) {
							method.invoke(listener, event);
						}
					}
				}
			}
		
		}
	}
	
	public void endInit() {
		
	}
	
	public List<Listener> getListeners() {
		return listeners;
	}
	
}
