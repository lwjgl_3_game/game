package fr.fcgame.client.renderer.renderers;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

import fr.fcgame.client.Game;
import fr.fcgame.client.components.Chunk;
import fr.fcgame.client.components.World;
import fr.fcgame.client.meshes.ChunkMeshType;
import fr.fcgame.client.renderer.shader.World3DObjectShader;
import fr.fcgame.client.utils.lwjgl.Helper;
import fr.fcgame.client.utils.lwjgl.Renderer;
import fr.fcgame.client.utils.lwjgl.Window;
import fr.fcgame.client.utils.lwjgl.model.IndicesMesh;
import fr.fcgame.client.utils.lwjgl.shaders.ShaderProgram;
import fr.fcgame.client.utils.lwjgl.texture.Texture;
import fr.fcgame.client.utils.math.MathUtils;
import fr.fcgame.client.utils.vao.Vao;

public abstract class ChunkCompoundRenderer extends Renderer<Chunk>{

	protected ChunkMeshType type;
	protected World world;
	protected int chunkRenderer;
	
	public ChunkCompoundRenderer(ChunkMeshType type) {
		super();
		this.type = type;
		this.world = Game.getGame().getWorld();
		this.chunkRenderer = 0;
	}

	@Override
	protected void injectMainUniforms(ShaderProgram shaderProgram) {
		World3DObjectShader shader = Game.getGame().getMainRenderer().getMyShader(this);
		Matrix4f view = MathUtils.createViewMatrix(Game.getGame().getCamera());
		shader.loadViewMatrix(view);
	}

	/** 
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param chunk Le chunk a mettre à jour
	 * @date 26/01/2020
	 */
	public abstract void updateChunk(Chunk chunk);
	
	@Override
	protected void injectForEachUniforms(Chunk type, ShaderProgram shaderProgram) {
		World3DObjectShader shader = Game.getGame().getMainRenderer().getMyShader(this);
		Matrix4f transformation = MathUtils.createTransformMatrix(
													new Vector3f(0, 0, 0),
													new Vector3f(0, 0, 0), 1f);
		shader.loadTransformationMatrix(transformation);
	}

	@Override
	protected void render(Game game, Window window, ShaderProgram shaderProgram) {
		// Chaque chunk utilise le même atlas de texture:
				// On apelle donc une seule fois la texture pour chaque modèle
				
				Texture blockAtlas = game.getWorld().getBlockAtlas();

				// On choisit la texture d'openGL
				GL13.glActiveTexture(GL13.GL_TEXTURE0);
				// On attribut à la texture 0 la texture de l'atlas de block
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, blockAtlas.getTextureID());
				this.chunkRenderer = 0;

				this.injectMainUniforms(shaderProgram);
				
				for(Chunk chunk : game.getWorld().getVisibleChunks(game.getCamera())) {
					if(!game.getWorld().insideFrustum(chunk)) {
						continue;
					}
					// Pour manipuler les données et les envoyées au GPU, on doit
					// d'abord dire au GPU de l'utiliser
					if(!chunk.hasMesh(this.type)) {
						this.updateChunk(chunk);
						continue;
					}
					IndicesMesh mesh = chunk.getChunkMesh(this.type);
					Vao vao = mesh.getVao();
					if(vao == null) {
						continue;
					}
					
					vao.bind();

					vao.enableVBOs();
					
					this.injectForEachUniforms(chunk, shaderProgram);
					
					mesh.drawMesh();
					this.chunkRenderer++;
					
					vao.disableVBOs();
					
					vao.unbind();
				}
				if(this.lastDisplayInformation + 1000 < Helper.getTime()) {
					this.lastDisplayInformation = Helper.getTime();
				}
				// On désactive la texture
				GL13.glActiveTexture(GL13.GL_TEXTURE0);
	}

	@Override
	public void init(Game game, Window window) {
		World3DObjectShader shader = Game.getGame().getMainRenderer().getMyShader(this);
		shader.loadProjection(game.getCamera().getProjectionMatrix());
	}

}
