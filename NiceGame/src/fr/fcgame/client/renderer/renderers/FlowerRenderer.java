package fr.fcgame.client.renderer.renderers;

import fr.fcgame.client.components.Chunk;
import fr.fcgame.client.meshes.ChunkMeshType;
import fr.fcgame.client.meshes.generators.ChunkMeshGenerator;
import fr.fcgame.client.meshes.generators.FlowerMeshGenerator;
import fr.fcgame.client.renderer.shader.defaut.FlowerShader;
import fr.fcgame.client.utils.lwjgl.Helper;
import fr.fcgame.client.utils.lwjgl.model.IndicesMesh;
import fr.fcgame.client.utils.lwjgl.shaders.ShaderProgram;
import fr.fcgame.client.utils.vao.Vao;
import fr.fcgame.client.renderer.shader.defaut.FlowerShader;

public class FlowerRenderer extends ChunkCompoundRenderer{

	private static final float DEFAULT_WAVE_SPEED = 0.4f;
	private static final float DEFAULT_WAVE_AMPLITUDE = 0.01f;
	private static final float DEFAULT_WAVE_INTENSITY = 0.01f;
	
	private float time = 0;
	private float waveSpeed;
	private float intensity;
	private float amplitude;
	
	public FlowerRenderer(FlowerShader flowerShader) {
		this(flowerShader,
				DEFAULT_WAVE_SPEED,
				DEFAULT_WAVE_AMPLITUDE,
				DEFAULT_WAVE_INTENSITY);
	}
	
	public FlowerRenderer(FlowerShader flowerShader, float waveSpeed, float intensity, float amplitude) {
		super(ChunkMeshType.FLOWER);
		this.waveSpeed = waveSpeed;
		this.intensity = intensity;
		this.amplitude = amplitude;
		setLinkedShader(FlowerShader.class);
	}

	@Override
	protected void injectMainUniforms(ShaderProgram shaderProgram) {
		super.injectMainUniforms(shaderProgram);
		this.time+=0.1*this.waveSpeed;
		this.time %= 1;
		((FlowerShader)shaderProgram).loadTime(this.time);
	}
	
	@Override
	protected void injectForEachUniforms(Chunk type, ShaderProgram shaderProgram) {
		super.injectForEachUniforms(type, shaderProgram);
	}

	@Override
	public void updateChunk(Chunk chunk) {
		IndicesMesh oldMesh = chunk.setChunkMesh(this.type, new FlowerMeshGenerator(chunk)
								   .createMesh());
		if(oldMesh != null) {
			Vao.deleteVAO(oldMesh.getVao());
		}
	}


}
