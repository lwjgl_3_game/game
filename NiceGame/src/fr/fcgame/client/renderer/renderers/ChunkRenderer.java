package fr.fcgame.client.renderer.renderers;

import fr.fcgame.client.components.Chunk;
import fr.fcgame.client.meshes.ChunkMeshType;
import fr.fcgame.client.meshes.generators.ChunkMeshGenerator;
import fr.fcgame.client.renderer.shader.defaut.ChunkShader;
import fr.fcgame.client.utils.lwjgl.model.IndicesMesh;
import fr.fcgame.client.utils.vao.Vao;

public class ChunkRenderer extends ChunkCompoundRenderer {

	public ChunkRenderer() {
		super(ChunkMeshType.RAW);
		setLinkedShader(ChunkShader.class);
	}

	@Override
	public void updateChunk(Chunk chunk) {
		IndicesMesh oldMesh = chunk.setChunkMesh(this.type, new ChunkMeshGenerator(chunk)
								   .createMesh());
		if(oldMesh != null) {
			Vao.deleteVAO(oldMesh.getVao());
		}
	}

}
