package fr.fcgame.client.renderer.renderers;

import java.io.IOException;

import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

import fr.fcgame.client.Game;
import fr.fcgame.client.renderer.shader.World3DObjectShader;
import fr.fcgame.client.renderer.shader.defaut.SkyboxShader;
import fr.fcgame.client.utils.lwjgl.Renderer;
import fr.fcgame.client.utils.lwjgl.Window;
import fr.fcgame.client.utils.lwjgl.model.VerticesMesh;
import fr.fcgame.client.utils.lwjgl.shaders.ShaderProgram;
import fr.fcgame.client.utils.lwjgl.texture.CubeMapTexture;
import fr.fcgame.client.utils.math.MathUtils;
import fr.fcgame.client.utils.vao.Vao;

public class SkyboxRenderer extends Renderer<Void>{
	
	private static final float SIZE = 250f;
	private static final String[] TEXTURE_FILES = {"right", "left", "top", "bottom", "back", "front"};
	private static final float[] VERTICES = {        
	    -SIZE,  SIZE, -SIZE,
	    -SIZE, -SIZE, -SIZE,
	    SIZE, -SIZE, -SIZE,
	     SIZE, -SIZE, -SIZE,
	     SIZE,  SIZE, -SIZE,
	    -SIZE,  SIZE, -SIZE,

	    -SIZE, -SIZE,  SIZE,
	    -SIZE, -SIZE, -SIZE,
	    -SIZE,  SIZE, -SIZE,
	    -SIZE,  SIZE, -SIZE,
	    -SIZE,  SIZE,  SIZE,
	    -SIZE, -SIZE,  SIZE,

	     SIZE, -SIZE, -SIZE,
	     SIZE, -SIZE,  SIZE,
	     SIZE,  SIZE,  SIZE,
	     SIZE,  SIZE,  SIZE,
	     SIZE,  SIZE, -SIZE,
	     SIZE, -SIZE, -SIZE,

	    -SIZE, -SIZE,  SIZE,
	    -SIZE,  SIZE,  SIZE,
	     SIZE,  SIZE,  SIZE,
	     SIZE,  SIZE,  SIZE,
	     SIZE, -SIZE,  SIZE,
	    -SIZE, -SIZE,  SIZE,

	    -SIZE,  SIZE, -SIZE,
	     SIZE,  SIZE, -SIZE,
	     SIZE,  SIZE,  SIZE,
	     SIZE,  SIZE,  SIZE,
	    -SIZE,  SIZE,  SIZE,
	    -SIZE,  SIZE, -SIZE,

	    -SIZE, -SIZE, -SIZE,
	    -SIZE, -SIZE,  SIZE,
	     SIZE, -SIZE, -SIZE,
	     SIZE, -SIZE, -SIZE,
	    -SIZE, -SIZE,  SIZE,
	     SIZE, -SIZE,  SIZE
	};
	
	private VerticesMesh mesh;
	private CubeMapTexture skyBoxTexture;
	
	public SkyboxRenderer(Game game, SkyboxShader shader) {
		Vao skyBoxVao = Vao.createVao();
		setLinkedShader(SkyboxShader.class);
		skyBoxVao.bind();
		skyBoxVao.bindData(VERTICES, 3);
		skyBoxVao.unbind();
		this.mesh = new VerticesMesh(skyBoxVao, VERTICES.length);
		try {
			this.skyBoxTexture = CubeMapTexture.createCubeMapAndAsign(TEXTURE_FILES);
		} catch (IOException e) {
			e.printStackTrace();
		}
		shader.start();
		shader.loadProjection(game.getCamera().getProjectionMatrix());
		shader.stop();
	}
	
	@Override
	protected void injectMainUniforms(ShaderProgram shaderProgram) {
		World3DObjectShader shader = Game.getGame().getMainRenderer().getMyShader(this);
		Matrix4f view = MathUtils.createViewMatrix(Game.getGame().getCamera());
		shader.loadViewMatrix(view);
	}

	@Override
	protected void injectForEachUniforms(Void type, ShaderProgram shaderProgram) {
		
	}

	@Override
	protected void render(Game game, Window window, ShaderProgram shaderProgram) {
		
		injectMainUniforms(shaderProgram);
		Vao meshVao = this.mesh.getVao();
		meshVao.bind();
		meshVao.enableVBOs();
		injectForEachUniforms(null, shaderProgram);
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		// On attribut à la texture 0 la texture de l'atlas de block
		GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, this.skyBoxTexture.getTextureID());
		GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, VERTICES.length/3);
		meshVao.disableVBOs();
		meshVao.unbind();
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
	}

	@Override
	public void init(Game game, Window window) {
	}

}
