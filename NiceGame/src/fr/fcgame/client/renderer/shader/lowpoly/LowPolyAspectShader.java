package fr.fcgame.client.renderer.shader.lowpoly;

import fr.fcgame.client.renderer.shader.World3DObjectShader;

public class LowPolyAspectShader extends World3DObjectShader{

	private static final String CURRENT_LOC = "resources/shaders/entity/lowpoly/";
	
	public LowPolyAspectShader() {
		super(CURRENT_LOC+"vertexShader.glsl", CURRENT_LOC+"geometryShader.glsl", CURRENT_LOC+"fragmentShader.glsl");
	}
	
	@Override
	protected void getAllUniformLocations() {
		super.getAllUniformLocations();
		registerUniform("lightPosition");
		registerUniform("lightColour");
	}
	
	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoords");
		super.bindAttribute(2, "light");
	}
	

	
	
}
