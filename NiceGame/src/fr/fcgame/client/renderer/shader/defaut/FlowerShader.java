package fr.fcgame.client.renderer.shader.defaut;

import fr.fcgame.client.renderer.shader.World3DObjectShader;

public class FlowerShader extends World3DObjectShader{

	private static final String CURRENT_LOC = "resources/shaders/entity/flower/";
	
	public FlowerShader() {
		super(CURRENT_LOC+"vertexShader.glsl", CURRENT_LOC+"fragmentShader.glsl");
	}
	
	@Override
	protected void getAllUniformLocations() {
		super.getAllUniformLocations();
		registerUniform("lightPosition");
		registerUniform("lightColour");
		registerUniform("time");
	}
	
	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoords");
		super.bindAttribute(2, "light");
	}

	public void loadTime(float time) {
		this.injectFloat(this.getUniformVariable("time"), time);
	}

}
