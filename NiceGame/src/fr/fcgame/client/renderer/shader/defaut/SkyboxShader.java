package fr.fcgame.client.renderer.shader.defaut;

import org.joml.Matrix4f;

import fr.fcgame.client.renderer.shader.World3DObjectShader;

public class SkyboxShader extends World3DObjectShader{


	private static final String CURRENT_LOC = "resources/shaders/skybox/";
	
	public SkyboxShader() {
		super(CURRENT_LOC+"vertexShader.glsl", CURRENT_LOC+"fragmentShader.glsl");
	}

	@Override
	protected void bindAttributes() {
		bindAttribute(0, "position");
	}
	
	@Override
	protected void getAllUniformLocations() {
		 registerUniform("viewMatrix");
		 registerUniform("projectionMatrix");
	}
	
	@Override
	public void loadTransformationMatrix(Matrix4f matrix4f) {}
	
	@Override
	public void loadViewMatrix(Matrix4f matrix) {
		matrix._m30(0);
        matrix._m31(0);
        matrix._m32(0);
		super.loadViewMatrix(matrix);
	}

}
