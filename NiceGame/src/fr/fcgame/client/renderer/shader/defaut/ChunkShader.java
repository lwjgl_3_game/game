package fr.fcgame.client.renderer.shader.defaut;

import fr.fcgame.client.renderer.shader.World3DObjectShader;

/**
 * Shader par défaut
 * @author ComminQ_Q (Computer)
 *
 * @date 21/12/2019
 */
public class ChunkShader extends World3DObjectShader{

	private static final String CURRENT_LOC = "resources/shaders/entity/chunk/";
	
	/**
	 * Constructeur
	 * @author ComminQ_Q (Computer)
	 *
	 * @param vertexFile
	 * @param fragmentFile
	 * @date 21/12/2019
	 */
	public ChunkShader() {
		super(CURRENT_LOC+"vertexShader.glsl", CURRENT_LOC+"fragmentShader.glsl");
	}
	
	@Override
	protected void getAllUniformLocations() {
		super.getAllUniformLocations();
		registerUniform("lightPosition");
		registerUniform("lightColour");
	}
	
	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoords");
		super.bindAttribute(2, "light");
	}

}
