package fr.fcgame.client.renderer.shader.defaut;

import java.awt.Color;

import org.joml.Vector3f;

import fr.fcgame.client.renderer.shader.World3DObjectShader;
import fr.fcgame.client.utils.Utils;

public class DefaultShader extends World3DObjectShader{

	private static final String CURRENT_LOC = "resources/shaders/def";
	
	public DefaultShader() {
		super(CURRENT_LOC+"vertexShader.glsl", CURRENT_LOC+"fragmentShader.glsl");
	}
	
	@Override
	protected void getAllUniformLocations() {
		super.getAllUniformLocations();
		registerUniform("colour");
	}
	
	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
	}
	
	public void loadColour(Color color) {
		loadColour(Utils.rgbToVec3f(color));
	}
	
	public void loadColour(Vector3f colour) {
		injectVector(getUniformLocation("colour"), colour);
	}

}
