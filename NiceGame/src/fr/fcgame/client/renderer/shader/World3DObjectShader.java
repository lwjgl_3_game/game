package fr.fcgame.client.renderer.shader;

import org.joml.Matrix4f;

import fr.fcgame.client.utils.lwjgl.shaders.ShaderProgram;

public abstract class World3DObjectShader extends ShaderProgram{

	public World3DObjectShader(String vertexFile, String geometryFile, String fragmentFile) {
		super(vertexFile, geometryFile, fragmentFile);
	}

	public World3DObjectShader(String vertexFile, String fragmentFile) {
		super(vertexFile, fragmentFile);
	}
	
	@Override
	protected void bindAttributes() {}
	
	@Override
	protected void getAllUniformLocations() {
		 registerUniform("transformationMatrix");
		 registerUniform("viewMatrix");
		 registerUniform("projectionMatrix");
	}
	
	
	public void loadProjection(Matrix4f matrix4f) {
		injectMatrix(getUniformLocation("projectionMatrix"), matrix4f);
	}
	
	public void loadTransformationMatrix(Matrix4f matrix4f) {
		injectMatrix(getUniformLocation("transformationMatrix"), matrix4f);
	}
	
	public void loadViewMatrix(Matrix4f matrix4f) {
		injectMatrix(getUniformLocation("viewMatrix"), matrix4f);
	}

	
	

}
