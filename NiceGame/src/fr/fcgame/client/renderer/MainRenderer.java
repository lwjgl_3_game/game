package fr.fcgame.client.renderer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.fcgame.client.Game;
import fr.fcgame.client.utils.lwjgl.Renderer;
import fr.fcgame.client.utils.lwjgl.Window;
import fr.fcgame.client.utils.lwjgl.shaders.ShaderProgram;

/**
 * Renderer principale
 * @author ComminQ_Q (Computer)
 *
 * @date 21/12/2019
 */
public class MainRenderer extends Renderer<Void>{

	private List<Renderer<?>> renderers; 
	private Map<Class<? extends ShaderProgram>, ShaderProgram> shaders; // La liste des shaders
	
	/**
	 * Constructeur
	 * @author ComminQ_Q (Computer)
	 *
	 * Initialise la liste à priorité ET la Map
	 *
	 * @date 21/12/2019
	 */
	public MainRenderer() {
		super();
		setPriority(PRIORITY_MONITOR);
		this.renderers = new ArrayList<Renderer<?>>();
		this.shaders = new HashMap<>();

	}
	
	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <T>
	 * @param renderer
	 * @return
	 * @date 23/12/2019
	 */
	public <T extends ShaderProgram> T getMyShader(Renderer<?> renderer){
		return (T) this.shaders.get(renderer.getLinkedShader());
	}
	
	/**
	 * Ajout un programme Shader au Système de rendu principal
	 * @author ComminQ_Q (Computer)
	 *
	 * @param program
	 * @date 21/12/2019
	 */
	public void registerShader(ShaderProgram program) {
		if(!this.shaders.containsKey(program.getClass())) {
			this.shaders.put(program.getClass(), program);
		}
		System.out.println("ShaderList :  "+getShaders());
	}
	
	/**
	 * Renvoie une Collection (vue) de Shader
	 * @author ComminQ_Q (Computer)
	 *
	 * @return
	 * @date 21/12/2019
	 */
	public Collection<ShaderProgram> getShaders() {
		return shaders.values();
	}
	
	@Override
	protected void render(Game game, Window window, ShaderProgram shaderProgram) {
		for(Renderer<?> renderer : this.renderers) {
			// Fait un rendu de chaque renderer
			// du plus important, au moins important
			ShaderProgram currentProgram = this.shaders.get(renderer.getLinkedShader());
			if(currentProgram == null) {
				System.out.println(renderer.getClass().getName()+" has no shader linked");
				renderer.renderScene(null);
			}else {
				currentProgram.start();
				renderer.renderScene(currentProgram);
				currentProgram.stop();
			}
			
		}
	}
	
	/**
	 * Nettoie tous les shader
	 * @author ComminQ_Q (Computer)
	 *
	 * @date 21/12/2019
	 */
	public void clean() {
		for(ShaderProgram shaderProgram : this.shaders.values()) {
			shaderProgram.cleanUp();
		}
	}
	
	/**
	 * Ajout un rendu à la liste des Renderer 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param renderer
	 * @date 21/12/2019
	 */
	public void add(Renderer<?> renderer) {
		if(this.renderers.contains(renderer)) {
			return;
		}
		this.renderers.add(renderer);
		System.out.println("RendererList : "+getRenderers());
	}

	@Override
	public void init(Game game, Window window) {
		for(Renderer<?> renderer : this.renderers) {
			ShaderProgram currentProgram = this.shaders.get(renderer.getLinkedShader());
			System.out.println("Init "+renderer.getClass()+" with "+currentProgram.getClass());
			if(currentProgram == null) {
				renderer.init(game, window);
			}else {
				currentProgram.start();
				renderer.init(game, window);
				currentProgram.stop();
			}
		}
	}

	public List<Renderer<?>> getRenderers() {
		return renderers;
	}
	
	@Override
	protected void injectMainUniforms(ShaderProgram shaderProgram) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void injectForEachUniforms(Void type, ShaderProgram shaderProgram) {
		// TODO Auto-generated method stub
		
	}

}
