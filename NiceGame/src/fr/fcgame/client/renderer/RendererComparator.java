package fr.fcgame.client.renderer;

import java.util.Comparator;

import fr.fcgame.client.utils.lwjgl.Renderer;

/**
 * 
 * @author ComminQ_Q (Computer)
 *
 * Identifie la render le plus prioritéaire
 *
 * @date 21/12/2019
 */
public class RendererComparator implements Comparator<Renderer<?>>{

	@Override
	public int compare(Renderer<?> r1, Renderer<?> r2) {
		return r2.getPriority() - r1.getPriority();
	}

}
