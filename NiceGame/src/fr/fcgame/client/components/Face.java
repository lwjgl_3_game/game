package fr.fcgame.client.components;

import java.util.Arrays;
import java.util.List;

import org.joml.Vector3i;

public enum Face {

	TOP(0, 1, 0),
	BOTTOM(0, -1, 0),
	RIGHT(1, 0, 0),
	LEFT(-1, 0, 0),
	BACK(0, 0, -1),
	FRONT(0, 0, 1);
	
	public static final List<Face> SIDES = Arrays.asList(RIGHT, LEFT, BACK, FRONT);
	
	private Vector3i adjacent;
	
	private Face() {
		this(0, 0, 0);
	}
	
	private Face(int x, int y, int z) {
		this.adjacent = new Vector3i();
		this.adjacent.x = x;
		this.adjacent.y = y;
		this.adjacent.z = z;
	}
	
}

