package fr.fcgame.client.components;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.joml.Vector2i;
import org.joml.Vector3f;

import fr.fcgame.client.Game;
import fr.fcgame.client.components.blocks.Block;
import fr.fcgame.client.components.material.Material;
import fr.fcgame.client.meshes.ChunkMeshType;
import fr.fcgame.client.utils.game.AABB;
import fr.fcgame.client.utils.lwjgl.model.IndicesMesh;
import fr.fcgame.client.utils.math.MathUtils;
import fr.fcgame.client.utils.noise.NoiseOptions;

public class Chunk {

	private Block[] blocks;
	private final int x, z;
	private AABB axisAlignedBoundingBoxed;
	private Map<ChunkMeshType, IndicesMesh> chunkMeshes;
	private int[][] heightMap;
	
	public Chunk(int x, int z, World world) {
		this.x = x;
		this.z = z;
		this.blocks = new Block[World.BLOCKS_PER_CHUNK];
		this.heightMap = new int[World.CHUNK_SIZE][World.CHUNK_SIZE];
		this.chunkMeshes = new HashMap<>();
		this.axisAlignedBoundingBoxed = new AABB(
					new Vector3f(
								x * World.CHUNK_SIZE,
								0,
								z * World.CHUNK_SIZE
							),
					new Vector3f(
								x * World.CHUNK_SIZE + (x < 0 ? - World.CHUNK_SIZE : + World.CHUNK_SIZE),
								World.CHUNK_HEIGHT,
								z * World.CHUNK_SIZE + (z < 0 ? - World.CHUNK_SIZE : + World.CHUNK_SIZE)
							)
				);
		
		for (int x_ = 0; x_ < World.CHUNK_SIZE; x_++) {
			for (int z_ = 0; z_ < World.CHUNK_SIZE; z_++) {
				
				int trueX = this.getX() * World.CHUNK_SIZE + x_
				  , trueZ = this.getZ() * World.CHUNK_SIZE + z_;

				NoiseOptions secondNoise = world.firstNoise;
		        float result = world.getOctaveNoiseAt(trueX, trueZ, secondNoise);
		        //float result = world.getNoiseAt(trueX, trueZ);
				float maxY = (result * secondNoise.amplitude + secondNoise.offset);
				int i_maxY=MathUtils.floor(maxY);
				this.heightMap[x_][z_] = i_maxY;
			}
		}
		
		initChunk(world);
	}
	
	public Block[] getBlocks() {
		return blocks;
	}
	
	public boolean hasMesh(ChunkMeshType type) {
		if(this.chunkMeshes.get(type) == null) {
			return false;
		}
		if(!this.chunkMeshes.get(type).hasVao()) {
			return false;
		}
		return true;
	}
	
	public int[][] getHeightMap() {
		return heightMap;
	}
	
	public void initChunk(World world) {
		
		
		for (int x = 0; x < World.CHUNK_SIZE; x++) {
			for (int z = 0; z < World.CHUNK_SIZE; z++) {
				
				int trueX = this.getX() * World.CHUNK_SIZE + x
				  , trueZ = this.getZ() * World.CHUNK_SIZE + z;

				int i_maxY=this.heightMap[x][z];
				
				for (int y = 0; y < World.CHUNK_HEIGHT; y++) {
					int indexInArray = y * World.CHUNK_SIZE * World.CHUNK_SIZE + z * World.CHUNK_SIZE + x;
					Block b = new Block(trueX, y, trueZ);
					b.setType(Material.AIR);
					
					if(y <= i_maxY) {
						if(y == i_maxY) {
							b.setType(Material.GRASS);
						}else if(y >= i_maxY - 3) {
							b.setType(Material.DIRT);
						}else if(y == 0) {
							b.setType(Material.BEDROCK);
						}else {
							b.setType(Material.STONE);
						}
					}
					
					this.blocks[indexInArray] = b;
				}
			}
		}
	}
	
	public int getX() {
		return x;
	}
	
	public int getZ() {
		return z;
	}
	
	public Block getBlockRelative(int x, int y, int z) {
		return this.blocks[indexInArray(x, y, z)];
	}
	
	public int indexInArray(int x, int y, int z) {
		int index = (y) * World.CHUNK_SIZE * World.CHUNK_SIZE
				 +  (z) * World.CHUNK_SIZE
				 +  (x);
		return index;
	}
	
	public Vector2i getPosition() {
		return new Vector2i(this.x, this.z);
	}
	
	public int centerY() {
		return (World.CHUNK_HEIGHT) / 2;
	}
	
	public int centerX() {
		return this.x < 0 ? this.x * World.CHUNK_SIZE - (World.CHUNK_SIZE / 2) : this.x * World.CHUNK_SIZE + (World.CHUNK_SIZE / 2);
	}
	
	public int centerZ() {
		return this.z < 0 ? this.z * World.CHUNK_SIZE - (World.CHUNK_SIZE / 2) : this.z * World.CHUNK_SIZE + (World.CHUNK_SIZE / 2);
	}
	
	public IndicesMesh getChunkMesh(ChunkMeshType type) {
		return this.chunkMeshes.get(type);
	}
	
	public IndicesMesh setChunkMesh(ChunkMeshType type, IndicesMesh mesh) {
		return this.chunkMeshes.put(type, mesh);
	}
	
	public Map<ChunkMeshType, IndicesMesh> getChunkMeshes() {
		return this.chunkMeshes;
	}
	
	@Override
	public String toString() {
		return "Chunk[x = "+x+", z = "+z+"]";
	}

	public AABB getAxisAlignedBoundingBoxed() {
		return axisAlignedBoundingBoxed;
	}

	public void update() {
		Game.getGame().getWorld().addChunkUpdate(this);
	}
	
	
	
	
}
