package fr.fcgame.client.components.material;

import java.util.HashMap;
import java.util.Map;

import fr.fcgame.client.components.Face;
import fr.fcgame.client.utils.lwjgl.texture.Texture;
import fr.fcgame.client.utils.math.MathUtils;

public enum Material {
	
	AIR((byte)			0xFF),
	DIRT((byte)			0x00),
	GRASS((byte)		0x01),
	GRASS_TOP((byte)	0x02),
	STONE((byte) 		0x03),
	SAND((byte) 		0x04),
	BEDROCK((byte)		0x05),
	ROSE((byte)			0x06),
	MUSHROOM((byte)		0x07),
	LOG((byte)			0x08),
	INSIDE_LOG((byte) 	0x09);
	
	private static Map<Byte, Material> idMaterialMap = new HashMap<>();
	public static Texture TEXTURE_ATLAS;
	public static final int WIDTH_IMAGE = 8;
	public static final int WIDTH_ATLAS = 64;
	public static final int IMAGE_PER_ROW_COLUMN = WIDTH_ATLAS / WIDTH_IMAGE;
	
	public static final float WIDTH_IN_UV = mapToUV(WIDTH_IMAGE);
	
	private static float mapToUV(int x_px) {
		return MathUtils.map(x_px, 0, 64, 0, 1);
	}
	
	static {
		for(Material material : Material.values()) {
			idMaterialMap.put(material.getIdentifier(), material);
		}
	}
	
	public static void initAll() {
		int currentTexture = 0;
		for(Material material : Material.values()) {
			if(material == AIR)continue;
			int x = currentTexture % IMAGE_PER_ROW_COLUMN;
			int y = (int) Math.floor(currentTexture / IMAGE_PER_ROW_COLUMN);
			float xUV = x * WIDTH_IN_UV;
			float yUV = y * WIDTH_IN_UV;
			material.uvCoords = new HashMap<>();
			for(Face face : Face.values()) {
				material.uvCoords.put(face, new float[] {
						// top left
						xUV, yUV,
					    // top right
						xUV + WIDTH_IN_UV, yUV,
					    // bot  right
						xUV + WIDTH_IN_UV, yUV + WIDTH_IN_UV,
					    // bot left
						xUV, yUV+ WIDTH_IN_UV,
						
					});
			}
			currentTexture++;
		}
		// custom mapping
		// grass mapping
		Material.GRASS.uvCoords.replace(Face.TOP, Material.GRASS_TOP.uvCoords.get(Face.TOP));
		Material.GRASS.uvCoords.replace(Face.BOTTOM, Material.DIRT.uvCoords.get(Face.BOTTOM));
		

		Material.LOG.uvCoords.replace(Face.TOP, Material.INSIDE_LOG.uvCoords.get(Face.TOP));
		Material.LOG.uvCoords.replace(Face.BOTTOM, Material.INSIDE_LOG.uvCoords.get(Face.BOTTOM));
	}
	
	public static Material getByIdentifier(byte b) {
		return idMaterialMap.get(b);
	}
	
	private final byte identifier;
	private Map<Face, float[]> uvCoords;
	
	private Material(byte b) {
		this.identifier = b;
	}
	
	public boolean isFlower() {
		switch(this) {
		case MUSHROOM:
		case ROSE:
			return true;
		default:
			return false;
		}
	}
	
	public boolean isOpaque() {
		switch(this) {
		case MUSHROOM:
		case AIR:
		case ROSE:
			return false;
		default:
			return true;
		}
	}
	
	public Map<Face, float[]> getUVCoords() {
		return uvCoords;
	}
	
	public byte getIdentifier() {
		return identifier;
	}
	
}
