package fr.fcgame.client.components.blocks;

import java.util.Objects;

import org.joml.Vector3i;

import fr.fcgame.client.Game;
import fr.fcgame.client.components.Chunk;
import fr.fcgame.client.components.World;
import fr.fcgame.client.components.material.Material;
import fr.fcgame.client.utils.game.AABB;
import fr.fcgame.client.utils.math.MathUtils;

public class Block {

	private static AABB aabb = new AABB();
	
	public static AABB toAABB(Block block) {
		aabb.getMin().set(block.blockPosition.x, block.blockPosition.y, block.blockPosition.z);
		aabb.getMax().set(block.blockPosition.x+1, block.blockPosition.y+1, block.blockPosition.y);
		return aabb.clone();
	}
	
	public static Block createBlock(int x, int y, int z) {
		return createBlock(x, y, z, Material.STONE);
	}

	public static Block createBlock(int x, int y, int z, Material material) {
		Block block = new Block(x, y, z);
		block.setType(material);
		return block;
	}
	
	public static void updateBlock(Block block) {
		World world = Game.getGame().getWorld();
		Chunk currentChunk = block.getChunk();
		
		if(currentChunk == null)return;
		
		int indexInArray = world.indexInArray(block.blockPosition.x, block.blockPosition.y, block.blockPosition.z);
		if(indexInArray < 0 || indexInArray > World.BLOCKS_PER_CHUNK) return;
		
		Block updatedBlock = null;
		//TODO recalculer le blocks  en fonction de son nouveau matériel
		
		
		currentChunk.getBlocks()[indexInArray] = updatedBlock;
	}

	private Vector3i blockPosition;
	private byte materialID;

	public Block(Vector3i vec) {
		this(vec.x, vec.y, vec.z);
	}
	
	public Block(int x, int y, int z) {
		this.blockPosition = new Vector3i(x, y, z);
		this.materialID = Material.GRASS.getIdentifier();
	}

	public Block getBlockAbove() {
		return Game.getGame().getWorld().getBlockAt(this.blockPosition.x, this.blockPosition.y + 1, this.blockPosition.z);
	}
	
	public void setType(Material material) {
		this.materialID = material.getIdentifier();
	}

	public Material getType() {
		return Material.getByIdentifier(this.materialID);
	}

	public byte getMaterialID() {
		return materialID;
	}

	public Vector3i getBlockPosition() {
		return this.blockPosition;
	}

	public void setBlockPosition(Vector3i blockPosition) {
		this.blockPosition = blockPosition;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(blockPosition);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Block)) {
			return false;
		}
		Block other = (Block) obj;
		return Objects.equals(blockPosition, other.blockPosition);
	}

	@Override
	public String toString() {
		return "Block [blockPosition=" + blockPosition.toString(MathUtils.FORMAT2F) + ", material=" + getType()
				+ "]";
	}

	public Chunk getChunk() {
		return Game.getGame().getWorld().getChunkAtBlock(this.blockPosition.x, this.blockPosition.z);
	}

}
