package fr.fcgame.client.components.world;

import java.util.Random;

import org.joml.Vector3i;

import fr.fcgame.client.components.Chunk;
import fr.fcgame.client.components.World;
import fr.fcgame.client.components.blocks.Block;
import fr.fcgame.client.components.material.Material;
import fr.fcgame.client.utils.Utils;
import fr.fcgame.client.utils.game.TerrainEdit;
import fr.fcgame.client.utils.math.MathUtils;

public class CaveGenerator {

	private World world;
	private int caveCount;

	public CaveGenerator(World world) {
		this.world = world;
		int width = World.CHUNK_SIZE * World.CHUNK_COUNT;
		this.caveCount = width * World.CHUNK_HEIGHT * width / 8192;
	}

	public void generateCave() {

		Random r = this.world.getRandom();
		Vector3i vec = Utils.VEC3I_BUFFER;
		int min = -World.CHUNK_SIZE * World.CHUNK_COUNT;
		int max = World.CHUNK_SIZE * World.CHUNK_COUNT;
		vec.set(MathUtils.randomInt(r, min, max), MathUtils.randomInt(r, 0, World.CHUNK_HEIGHT),
				MathUtils.randomInt(r, min, max));
		int caveLength = (int) (r.nextFloat() * r.nextFloat() * 200);
		System.out.println("cave length is " + caveLength);
		float caveRadius = r.nextFloat() * r.nextFloat();
		float theta = (float) (r.nextFloat() * Math.PI * 2);
		float phi = (float) (r.nextFloat() * Math.PI * 2);
		float deltaTheta = 0f;
		float deltaPhi = 0f;
		for (int i = 0; i < caveLength; i++) {
			vec.x += Math.sin(theta) * Math.cos(phi);
			vec.y += Math.cos(theta) * Math.cos(phi);
			vec.z += Math.sin(phi);

			theta += deltaTheta * 0.2;
			float newDeltaTheta = (deltaTheta * 0.9f) + r.nextFloat() - r.nextFloat();
			deltaTheta = newDeltaTheta;
			float newPhi = phi / 2 + deltaPhi / 4f;
			phi = newPhi;
			float newDeltaPhi = (deltaPhi * 0.75f) + r.nextFloat() - r.nextFloat();
			deltaPhi = newDeltaPhi;
			if (r.nextFloat() >= 0.25) {
				Vector3i centerPos = Utils.VEC3I_BUFFER1
						.set((int) (vec.x + (r.nextInt(4) - 2) * 0.2f),
							 (int) (vec.y + (r.nextInt(4) - 2) * 0.2f),
							 (int) (vec.z + (r.nextInt(4) - 2) * 0.2f));

				float radius = (World.CHUNK_HEIGHT - centerPos.y) / (float) World.CHUNK_HEIGHT;
				radius = 1.2f + (radius * 3.5f + 1) * caveRadius;
				radius *= Math.sin(i * Math.PI / caveLength);

				TerrainEdit.filleOblateSpheroid(this.world, centerPos, radius, Material.AIR);
			}
		}
	}

	public void generateCaves() {
		System.out.println("Generating " + this.caveCount + " caves");
		for (int i = 0; i < this.caveCount; i++) {
			generateCave();
		}
		for (Chunk c : this.world.getChunks()) {
			this.world.addChunkUpdate(c);
		}
	}
}
