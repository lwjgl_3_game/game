package fr.fcgame.client.components.world;

import fr.fcgame.client.components.Chunk;
import fr.fcgame.client.components.World;

public interface WorldComponent {

	public boolean hasToGenerate(World world, Chunk chunk, int x, int z);
	
	public void generate(World world, Chunk chunk, int x, int y, int z);
	
}
