package fr.fcgame.client.components.world;

import java.util.Random;

import org.joml.Vector3i;

import fr.fcgame.client.components.Chunk;
import fr.fcgame.client.components.World;
import fr.fcgame.client.components.blocks.Block;
import fr.fcgame.client.components.material.Material;
import fr.fcgame.client.utils.math.MathUtils;

public class TreeComponent implements WorldComponent{
	
	private Random random;
	
	public TreeComponent(long seed) {
		this.random = new Random(seed);
	}
	
	@Override
	public boolean hasToGenerate(World world, Chunk chunk, int x, int z) {
		int y = chunk.getHeightMap()[x][z]+1;
		Block block = chunk.getBlockRelative(x, y, z);
		Vector3i pos = block.getBlockPosition();

		boolean left = world.getBlockTypeAt(pos.x+1, y, pos.z) == Material.AIR;
		boolean right = world.getBlockTypeAt(pos.x-1, y, pos.z) == Material.AIR;
		boolean back = world.getBlockTypeAt(pos.x, y, pos.z+1) == Material.AIR;
		boolean front = world.getBlockTypeAt(pos.x, y, pos.z-1) == Material.AIR;
		return left && right && back && front && this.random.nextFloat() < 0.005f;
	}

	@Override
	public void generate(World world, Chunk chunk, int x, int y, int z) {
		
		int minY = 0;
		int maxY = MathUtils.randomInt(this.random, 6, 8);

		int leavesSize = 2;
		int minLeaves = maxY - leavesSize;
		
		for(int lx = -leavesSize; lx < leavesSize+1; lx++) {
			for(int lz = -leavesSize; lz < leavesSize+1; lz++) {
				for(int ly = minLeaves; ly < maxY; ly++) {
					world.getBlockAt(x+lx, y+ly, z+lz).setType(Material.SAND);
				}
			}
		}

		world.getBlockAt(x-leavesSize, minLeaves+y, z+leavesSize).setType(Material.AIR);
		world.getBlockAt(x-leavesSize, minLeaves+y+1, z+leavesSize).setType(Material.AIR);
		

		world.getBlockAt(x+leavesSize, minLeaves+y, z+leavesSize).setType(Material.AIR);
		world.getBlockAt(x+leavesSize, minLeaves+y+1, z+leavesSize).setType(Material.AIR);
		
		world.getBlockAt(x-leavesSize, minLeaves+y, z-leavesSize).setType(Material.AIR);
		world.getBlockAt(x-leavesSize, minLeaves+y+1, z-leavesSize).setType(Material.AIR);

		world.getBlockAt(x+leavesSize, minLeaves+y, z-leavesSize).setType(Material.AIR);
		world.getBlockAt(x+leavesSize, minLeaves+y+1, z-leavesSize).setType(Material.AIR);
		
		for(int i = minY; i < maxY; i++) {
			world.getBlockAt(x, y+i, z).setType(Material.LOG);
		}
	}

}
