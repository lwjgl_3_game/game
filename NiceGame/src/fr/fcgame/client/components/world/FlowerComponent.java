package fr.fcgame.client.components.world;

import java.util.Random;

import fr.fcgame.client.components.Chunk;
import fr.fcgame.client.components.World;
import fr.fcgame.client.components.material.Material;

public class FlowerComponent implements WorldComponent {

	@Override
	public boolean hasToGenerate(World world, Chunk chunk, int x, int z) {
		Random random = world.getRandom();
		return chunk
				.getBlockRelative(x, chunk.getHeightMap()[x][z]+1, z).getType() == Material.AIR
				&& random.nextFloat() < 0.01f;
	}

	@Override
	public void generate(World world, Chunk chunk, int x, int y, int z) {
		world.getBlockAt(x, y+1, z).setType(Material.ROSE);
	}

}
