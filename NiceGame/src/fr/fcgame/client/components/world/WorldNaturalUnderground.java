package fr.fcgame.client.components.world;

import fr.fcgame.client.components.World;

public interface WorldNaturalUnderground extends WorldNatural {

	public void generateNaturalUnderground(World world);
	
}
