package fr.fcgame.client.components;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.joml.FrustumIntersection;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector3i;

import fr.fcgame.client.Game;
import fr.fcgame.client.components.blocks.Block;
import fr.fcgame.client.components.material.Material;
import fr.fcgame.client.components.world.CaveGenerator;
import fr.fcgame.client.components.world.FlowerComponent;
import fr.fcgame.client.components.world.TreeComponent;
import fr.fcgame.client.components.world.WorldComponent;
import fr.fcgame.client.entity.Entity;
import fr.fcgame.client.meshes.ChunkMeshType;
import fr.fcgame.client.renderer.renderers.ChunkRenderer;
import fr.fcgame.client.utils.Utils;
import fr.fcgame.client.utils.java.Pair;
import fr.fcgame.client.utils.lwjgl.Camera;
import fr.fcgame.client.utils.lwjgl.texture.Texture;
import fr.fcgame.client.utils.math.MathUtils;
import fr.fcgame.client.utils.noise.NoiseOptions;
import fr.fcgame.client.utils.noise.OpenSimplexNoise;

public class World {

	public static Pair<Integer, Integer> PAIR_BUFFER = new Pair<Integer, Integer>(0, 0);
	
	public static final int CHUNK_SIZE = 16;
	public static final int LEAST_HEIGHT = 0;
	public static final int CHUNK_HEIGHT = 128 ;
	public static final int WATER_LEVEL = 12;

	public static final int BLOCKS_PER_CHUNK = CHUNK_SIZE * CHUNK_SIZE * CHUNK_HEIGHT;

	public static final int CHUNK_COUNT = 4;

	public static final int MIN_WORLD_SIZE = (CHUNK_SIZE) * (CHUNK_COUNT) * -1;
	public static final int MAX_WORLD_SIZE = (CHUNK_SIZE) * (CHUNK_COUNT);
	private static final int RENDER_DISTANCE = 5;

	private static Block createAirBlock(int x, int y, int z) {
		Block air = new Block(x, y, z);
		air.setType(Material.AIR);
		return air;
	}

	public static Pair<Integer, Integer> getChunkXYAt(int x, int z) {
		int chunkX = x < 0 ? ((x - World.CHUNK_SIZE + 1) / World.CHUNK_SIZE) : (x / World.CHUNK_SIZE);
		int chunkZ = z < 0 ? ((z - World.CHUNK_SIZE + 1) / World.CHUNK_SIZE) : (z / World.CHUNK_SIZE);
		PAIR_BUFFER.set(chunkX, chunkZ);
		return PAIR_BUFFER;
	}

	private Map<Pair<Integer, Integer>, Chunk> chunks;
	private List<Chunk> visibleChunks;
	private List<Entity> entity;
	private Texture blockAtlas;
	private long seed = 152111456635L;
	private OpenSimplexNoise noiseGenerator;

	// Frustrum View Culling
	private FrustumIntersection frustumInt;
	private Matrix4f viewProjMatrix;
	private Random random;
	public NoiseOptions firstNoise;
	public NoiseOptions secondNoise;
	private Game game;
	private ChunkLoader chunkLoader;

	public World(Game game) {
		this.game = game;
		this.chunkLoader = new ChunkLoader();
		this.random = new Random(this.seed);
		this.chunks = new HashMap<>();
		this.frustumInt = new FrustumIntersection();
		this.viewProjMatrix = new Matrix4f();
		this.noiseGenerator = new OpenSimplexNoise(this.getSeed());
		this.entity = new ArrayList<Entity>();
		this.visibleChunks = new ArrayList<Chunk>();
		
		this.firstNoise = new NoiseOptions();
		firstNoise.amplitude = 25;
		firstNoise.octaves = 6;
		firstNoise.smoothness = 205.f;
		firstNoise.roughness = 0.58f;
		firstNoise.offset = WATER_LEVEL;

		this.secondNoise = new NoiseOptions();
		secondNoise.amplitude = 75;
		secondNoise.octaves = 4;
		secondNoise.smoothness = 25;
		secondNoise.roughness = 0.04f;
		secondNoise.offset = 0;
		
		try {
			this.blockAtlas = Texture.createTextureAndAsign("resources/textures/blocks/blockatlas.png");
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (int x = -CHUNK_COUNT; x < CHUNK_COUNT; x++) {
			for (int z = -CHUNK_COUNT; z < CHUNK_COUNT; z++) {
				this.chunks.put(new Pair<>(x, z), new Chunk(x, z, this));
			}
		}
		List<WorldComponent> components = new ArrayList<>();
		components.add(new TreeComponent(this.seed));
		components.add(new FlowerComponent());
		for(Chunk chunk : this.getChunks()) {
			for(int x = 0; x < CHUNK_SIZE; x++) {
				for(int z = 0; z < CHUNK_SIZE; z++) {
					int maxHeight = chunk.getHeightMap()[x][z];
					Block block = chunk.getBlockRelative(x, maxHeight, z);
					Vector3i realPosition = block.getBlockPosition();
					for(WorldComponent component : components) {
						if(component.hasToGenerate(this, chunk, x, z)) {
							component.generate(this, chunk, realPosition.x, maxHeight, realPosition.z);
						}
					}
					
				}
			}
			addChunkUpdate(chunk);
		}
		
		// Cave generation
		
		
		new CaveGenerator(this).generateCaves();
	}

	public float getNoiseAt(int x, int z) {
		return (float) this.noiseGenerator.eval(x/64.0f, z/64.0f);
	}
	
	public float getNoiseAt(float x, float y, float z) {
		return (float) this.noiseGenerator.eval(x, y, z);
	}
	
	public float getOctaveNoiseAt(int _x, int _z, NoiseOptions noiseOptions) {
		// Get voxel X/Z positions
		float voxelX = _x;
		float voxelZ = _z;

		// Begin iterating through the octaves
		float value = 0;
		float accumulatedAmps = 0;
		for (int i = 0; i < noiseOptions.octaves; i++) {
			float frequency = (float) Math.pow(2.0f, i);
			float amplitude = (float) Math.pow(noiseOptions.roughness, i);

			float x = voxelX * frequency / noiseOptions.smoothness;
			float y = voxelZ * frequency / noiseOptions.smoothness;

			float noise = (float) this.noiseGenerator.eval(x, y);
			noise = (noise + 1.0f) / 2.0f;
			value += noise * amplitude;
			accumulatedAmps += amplitude;
		}
		return value / accumulatedAmps;
	}

	public List<Chunk> getVisibleChunkFrom(Vector3f position, int distance) {
		return null;
	}

	public void updateFrustumsPlanes(Camera camera) {
		this.viewProjMatrix.set(camera.getProjectionMatrix());
		this.viewProjMatrix.mul(MathUtils.createViewMatrix(camera));
		// Get frustum planes
		this.frustumInt.set(this.viewProjMatrix);
	}

	public boolean insideFrustum(Chunk chunk) {
		return true;
		// return this.frustumInt.testSphere(chunk.centerX(), chunk.centerY(),
		// chunk.centerZ(), CHUNK_SIZE);
	}

	public Texture getBlockAtlas() {
		return blockAtlas;
	}

	public List<Entity> getEntities() {
		return entity;
	}

	public void loadAyncChunk(int x, int z) {
		Pair<Integer, Integer> xz = new Pair<>(x, z);
		if(this.chunks.containsKey(xz)) {
			return;
		}
		this.chunkLoader
			.loadChunkAt(this, x, z)
			.thenAccept(chunk -> {
				this.chunks.put(xz, chunk);
			});
	}
	
	private List<int[]> buffer = new ArrayList<int[]>();
	
	public List<int[]> mustBeIn(Camera camera){
		
		return buffer;
	}
	
	public boolean isOutSide(Chunk chunk, Vector3f pos) {
		Utils.VEC3_BUFFER.set(chunk.getX(), 0, chunk.getZ());
		Utils.VEC3_BUFFER1.set(pos.x, 0, pos.z);

		int posX = MathUtils.Fast.round(Utils.VEC3_BUFFER1.x);
		int posZ = MathUtils.Fast.round(Utils.VEC3_BUFFER1.z);
		
		int chunkX = posX < 0 ? ((posX - World.CHUNK_SIZE + 1) / World.CHUNK_SIZE) : (posX / World.CHUNK_SIZE);
		int chunkZ = posZ < 0 ? ((posZ - World.CHUNK_SIZE + 1) / World.CHUNK_SIZE) : (posZ / World.CHUNK_SIZE);
		
		Utils.VEC3_BUFFER1.set(chunkX, 0, chunkZ);
		float dist = Utils.VEC3_BUFFER.distance(Utils.VEC3_BUFFER1);
		return dist > (RENDER_DISTANCE);
	}
	
	public void initChunkModels(ChunkRenderer renderer) {
		System.out.println("ChunkSize = " + this.chunks.size());
		long start = System.currentTimeMillis();
		for (Chunk chunk : this.chunks.values()) {
			for (ChunkMeshType type : ChunkMeshType.values()) {
				try {
					type.getMeshGenerator()
						.getConstructor(Chunk.class)
						.newInstance(chunk).createMesh();
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
			}
			this.visibleChunks.add(chunk);
			continue;
		}
		long end = System.currentTimeMillis();
		System.gc();
		System.out.println("Build " + (CHUNK_COUNT * CHUNK_COUNT) + "chunks in " + (end - start) + "ms");

	}

	public Material getBlockTypeAt(Vector3i vec) {
		return this.getBlockTypeAt(vec.x, vec.y, vec.z);
	}

	public Chunk getChunkAt(float x, float z) {
		return getChunkAt(MathUtils.floor(x), MathUtils.floor(z));
	}

	public Chunk getChunkAt(int x, int z) {
		PAIR_BUFFER.set(x, z);
		Chunk c = getChunkMap().get(PAIR_BUFFER);
		if (c == null)
			return null;
		return c;
	}

	public Chunk getChunkAtBlock(float x, float z) {
		return getChunkAtBlock(MathUtils.floor(x), MathUtils.floor(z));
	}

	public Chunk getChunkAtBlock(int x, int z) {
		Pair<Integer, Integer> chunkCoords = getChunkXYAt(x, z);
		Chunk c = getChunkMap().get(chunkCoords);
		if (c == null)
			return null;
		return c;
	}

	public int indexInArray(int x, int y, int z) {
		Chunk c = getChunkAtBlock(x, z);
		int exactX = x < 0 ? (World.CHUNK_SIZE * -c.getX() + x) : x;
		int exactZ = z < 0 ? (World.CHUNK_SIZE * -c.getZ() + z) : z;
		int index = (y) * World.CHUNK_SIZE * World.CHUNK_SIZE + (exactZ % World.CHUNK_SIZE) * World.CHUNK_SIZE
				+ (exactX % World.CHUNK_SIZE);
		return index;
	}

	public Material getBlockTypeAt(float x, float y, float z) {
		return getBlockTypeAt(x < 0 ? MathUtils.floor(x) : MathUtils.ceil(x), MathUtils.floor(y),
				x < 0 ? MathUtils.floor(z) : MathUtils.ceil(z));
	}

	public Material getBlockTypeAt(int x, int y, int z) {
		if (y < 0 || y > CHUNK_HEIGHT) {
			return Material.AIR;
		}

		Chunk toFind = getChunkAtBlock(x, z);
		if (toFind == null) {
			return Material.AIR;
		}

		int indexInArray = indexInArray(x, y, z);
		if (indexInArray < 0 || indexInArray >= BLOCKS_PER_CHUNK) {
			if (indexInArray < 0) {
				System.out.println("index in array(" + indexInArray + ") is incorrect (Size:" + BLOCKS_PER_CHUNK + ")");
				System.out.println("Block Pos : x=" + x + " y=" + y + " z=" + z);
			}
			return Material.AIR;
		}
		Material material = toFind.getBlocks()[indexInArray].getType();
		return material;
	}

	public Vector3i getRelativeCoords(Vector3i vec) {
		int x = vec.x % CHUNK_SIZE, y = vec.y, z = vec.z % CHUNK_SIZE;
		if (x < 0)
			x += CHUNK_SIZE;
		if (z < 0)
			z += CHUNK_SIZE;
		return new Vector3i(x, y, z);
	}

	public Block getBlockAt(Vector3i vec) {
		return this.getBlockAt(vec.x, vec.y, vec.z);
	}

	public Block getBlockAt(int x, int y, int z) {
		if (y < 0 || y > CHUNK_HEIGHT) {
			return createAirBlock(x, y, z);
		}
		Chunk toFind = getChunkAtBlock(x, z);
		if (toFind == null) {
			return createAirBlock(x, y, z);
		}
		int indexInArray = indexInArray(x, y, z);
		if (indexInArray < 0 || indexInArray > BLOCKS_PER_CHUNK) {
			System.out.println("block out of bound");
			return createAirBlock(x, y, z);
		}
		return toFind.getBlocks()[indexInArray];
	}

	public long getSeed() {
		return seed;
	}

	public Map<Pair<Integer, Integer>, Chunk> getChunkMap() {
		return chunks;
	}

	public Collection<Chunk> getChunks() {
		return this.chunks.values();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (Chunk chunk : this.chunks.values()) {
			builder.append(chunk + "\n");
		}
		return "World[Chunks = {" + "\n" + builder.toString() + "}]";
	}

	public void addChunkUpdate(Chunk chunk) {
		if (chunk == null) {
			System.out.println("Chunk is null");
			return;
		}
		System.out.println("Updating chunk...");
		this.game.updateChunk(chunk);
		System.out.println("Chunk is updated");
	}

	public List<Chunk> getVisibleChunks(Camera camera) {
		this.visibleChunks.clear();
		for(Chunk chunk : this.chunks.values()) {
			if(!isOutSide(chunk, camera.getPosition())) {
				this.visibleChunks.add(chunk);
			}
		}
		return this.visibleChunks;
	}

	public Random getRandom() {
		return this.random;
	}
}
