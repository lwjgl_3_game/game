package fr.fcgame.client.components;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import fr.fcgame.client.utils.java.Pair;

public class ChunkLoader{
	
	private Executor executors = Executors.newFixedThreadPool(4);
	
	public CompletableFuture<Chunk> loadChunkAt(World world, int x, int z){
		final Pair<Integer, Integer> xz = new Pair<>(x, z);
		world.getChunkMap().put(xz, null);
		return CompletableFuture.supplyAsync(() -> {
			Chunk chunk = new Chunk(x, z, world);
			return chunk;
		});
	}
	
}
