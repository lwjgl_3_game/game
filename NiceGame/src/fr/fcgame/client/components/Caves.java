package fr.fcgame.client.components;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector3i;

import fr.fcgame.client.utils.noise.OpenSimplexNoise;

public class Caves {

	private OpenSimplexNoise noiseX;
	private OpenSimplexNoise noiseY;
	private OpenSimplexNoise noiseZ;
	private float threshold;
	private float thresholdGeneration;
	private List<int[]> buffer;

	public Caves(long seedX, long seedY, long seedZ, float threshold, float thresholdGeneration) {
		super();
		this.noiseX = new OpenSimplexNoise(seedX);
		this.noiseY = new OpenSimplexNoise(seedY);
		this.noiseZ = new OpenSimplexNoise(seedZ);
		this.threshold = threshold;
		this.thresholdGeneration = thresholdGeneration;
		this.buffer = new ArrayList<int[]>();
	}
	
	public boolean hasToGenerate(World world, int x, int y, int z) {
		// Résultat sur l'intervalle [0;1]
		float result = (world.getNoiseAt(x, y, z)+1)/2;
		if(result > (1-this.thresholdGeneration))return true;
		return false;
	}
	
	public List<int[]> generateCave(int x, int y, int z){
		int oldX = x;
		int oldY = y;
		int oldZ = z;
		boolean finished = false;
		this.buffer.clear();
		while(!finished) {
			Vector3i v = getForPoint(oldX, oldY, oldZ);
			if(v.x == 0 && v.y == 0 && v.z == 0) {
				finished = true;
			}else {
				oldX+=v.x;
				oldY+=v.y;
				oldZ+=v.z;
				int[] coords = {oldX, oldY, oldZ};
				this.buffer.add(coords);
			}
		}
		return this.buffer;
	}
	
	public Vector3i getForPoint(int x, int y, int z) {
		double noiseXValue = this.noiseX.eval(x, 0);
		double noiseYValue = this.noiseY.eval(y, 0);
		double noiseZValue = this.noiseZ.eval(z, 0);
		return new Vector3i(
				transform(noiseXValue),
				transform(noiseYValue),
				transform(noiseZValue)
				);
	}
	
	public int transform(double val) {
		float minThreshold = -this.threshold;
		float maxThreshold = this.threshold;
		if(val > minThreshold && val < maxThreshold) {
			return 0;
		}else if(val < 0) {
			return -1;
		}
		return 1;
	}
	
	
	
}
