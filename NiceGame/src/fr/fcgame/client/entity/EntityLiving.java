package fr.fcgame.client.entity;

import org.joml.Vector2f;
import org.joml.Vector3f;

import fr.fcgame.client.Game;
import fr.fcgame.client.components.World;
import fr.fcgame.client.components.blocks.Block;
import fr.fcgame.client.components.material.Material;
import fr.fcgame.client.utils.game.AABB;
import fr.fcgame.client.utils.math.MathUtils;

public class EntityLiving extends Entity{

	protected float torque;
	protected Vector2f rotation;
	protected AABB boxCollision;
	protected Vector3f velocity;
	protected Vector3f acceleration;
	
	public EntityLiving() {
		super();
		this.rotation = new Vector2f();
		this.boxCollision = new AABB();
		this.velocity = new Vector3f();
		this.acceleration = new Vector3f();
	}
	
	public boolean isOnGround() {
		World world = Game.getGame().getWorld();
		Block block = world.getBlockAt(MathUtils.floor(super.position.x),
									   MathUtils.floor(super.position.y-1),
									   MathUtils.floor(super.position.z));
		if(block == null)return false;
		if(block.getMaterialID() == Material.AIR.getIdentifier())return false;
		return true;
	}
	
	public void processVelocity() {
		this.velocity.x -= this.torque;
		if(this.velocity.x < 0f)this.velocity.x = 0f;

		this.velocity.y -= this.torque;
		if(this.velocity.y < 0f)this.velocity.y = 0f;

		this.velocity.z -= this.torque;
		if(this.velocity.z < 0f)this.velocity.z = 0f;
	}
	
	public void processPosition() {
		this.position.x += this.velocity.x;
		this.position.y += this.velocity.y;
		this.position.z += this.velocity.z;
	}
	
	public Vector2f getRotation() {
		return rotation;
	}
	
	public AABB getBoxCollision() {
		return boxCollision;
	}
	
	public void setRotation(float pitch, float yaw) {
		this.rotation.x = pitch;
		this.rotation.y = yaw;
	}
	
	public void setBoxCollision(AABB boxCollision) {
		this.boxCollision = boxCollision;
	}

	public void translate(float translateX, float translateY, float translateZ) {
		this.position.x+=translateX;
		this.position.y+=translateY;
		this.position.z+=translateZ;
	}
	
	public void setVelocity(float x, float y, float z) {
		this.velocity.x = x;
		this.velocity.y = y;
		this.velocity.z = z;
	}
	
	public Vector3f getVelocity() {
		return velocity;
	}
	
	public Vector3f getAcceleration() {
		return acceleration;
	}
	
	
}
