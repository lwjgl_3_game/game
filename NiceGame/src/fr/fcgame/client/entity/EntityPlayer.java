package fr.fcgame.client.entity;

import org.joml.Vector3f;
import org.joml.Vector3i;

import fr.fcgame.client.utils.Utils;
import fr.fcgame.client.utils.math.MathUtils;

public class EntityPlayer extends EntityLiving{

	public static final float DEFAULT_PLAYER_EYE_LOC = 1.8f;
	public static final float DEFAULT_PLAYER_WIDTH = 1f;
	
	private float eyeLocation;
	private float playerHeight;
	private float playerWidth;

	public EntityPlayer() {
		super();
		this.eyeLocation = DEFAULT_PLAYER_EYE_LOC;
		this.playerWidth = DEFAULT_PLAYER_WIDTH;
		this.boxCollision.getMax().set(this.position.x + this.playerWidth/2, this.position.y, this.position.z + this.playerWidth/2);
		this.boxCollision.getMin().set(this.position.x - this.playerWidth/2, this.position.y-this.playerHeight, this.position.z - this.playerWidth/2);
	}
	

	public Vector3f getEyePosition(Vector3f buffer) {
		buffer.set(
				this.position.x,
				this.position.y+this.eyeLocation,
				this.position.z);
		return buffer;
	}
	
	public float getEyeLocation() {
		return eyeLocation;
	}
	
	public float getPlayerHeight() {
		return playerHeight;
	}
	
	public float getPlayerWidth() {
		return playerWidth;
	}
	
}
