package fr.fcgame.client.entity;

import org.joml.Vector3f;

public class Entity {

	protected Vector3f position;
	
	public Entity() {
		this.position = new Vector3f();
	}
	
	public Vector3f getPosition() {
		return position;
	}
	
	public void teleport(Vector3f position) {
		this.setPosition(position);
	}
	
	public void setPosition(Vector3f position) {
		this.position.x = position.x;
		this.position.y = position.y;
		this.position.z = position.z;
	}
	
}
