package fr.fcgame.client.utils.java;

import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class Streams {

	public static DoubleStream toStream(float[] floats) {
		return IntStream.range(0, floats.length)
                .mapToDouble(i -> floats[i]);
	}
	
}
