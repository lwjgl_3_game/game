package fr.fcgame.client.utils.java;

import java.util.Objects;

public class Pair<T1, T2> {

	private T1 t1;
	private T2 t2;

	public Pair(T1 t1, T2 t2) {
		super();
		this.t1 = t1;
		this.t2 = t2;
	}

	public T1 getT1() {
		return t1;
	}

	public void setT1(T1 t1) {
		this.t1 = t1;
	}

	public void set(T1 t1, T2 t2) {
		this.t1 = t1;
		this.t2 = t2;
	}

	public T2 getT2() {
		return t2;
	}

	public void setT2(T2 t2) {
		this.t2 = t2;
	}

	@Override
	public int hashCode() {
		return Objects.hash(t1, t2);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Pair)) {
			return false;
		}
		Pair<?, ?> other = (Pair<?, ?>) obj;
		return Objects.equals(t1, other.t1) && Objects.equals(t2, other.t2);
	}

	@Override
	public String toString() {
		return "Pair [t1=" + t1 + ", t2=" + t2 + "]";
	}

}
