package fr.fcgame.client.utils.java.wrappers;

public class IntegerWrapper extends NumberWrapper<Integer>{

	public IntegerWrapper(Integer value) {
		super(value);
	}

	@Override
	public void add(Integer value) {
		this.value+=value;
	}

	@Override
	public void sub(Integer value) {
		this.value-=value;
	}

}
