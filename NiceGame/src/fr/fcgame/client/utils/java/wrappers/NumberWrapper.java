package fr.fcgame.client.utils.java.wrappers;

public abstract class NumberWrapper<T extends Number> extends Wrapper<T>{

	public NumberWrapper(T value) {
		super(value);
	}

	public abstract void add(T value);
	
	public abstract void sub(T value);
	
}
