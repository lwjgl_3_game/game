package fr.fcgame.client.utils.java.wrappers;

public class Wrapper<T> {

	protected T value;
	
	public Wrapper(T value) {
		this.value = value;
	}
	
	public T getValue() {
		return value;
	}
	
	public void setValue(T value) {
		this.value = value;
	}
	
}
