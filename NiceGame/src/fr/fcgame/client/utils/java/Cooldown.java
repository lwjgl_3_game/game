package fr.fcgame.client.utils.java;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import fr.fcgame.client.utils.lwjgl.Helper;

public class Cooldown {

	private static Map<String, Cooldown> cooldowns = new HashMap<>();
	
	public static Cooldown getCooldown(String name) {
		return cooldowns.get(name);
	}
	
	public static boolean elapsed(String name) {
		if(!exist(name)) {
			return true;
		}
		Cooldown cooldown = getCooldown(name);
		return cooldown.last + cooldown.duration < Helper.getTime();
	}
	
	public static void create(String name, long duration) {
		if(exist(name))return;
		cooldowns.put(name, new Cooldown(name, duration));
	}
	
	public static void reset(String name) {
		if(!exist(name))return;
		getCooldown(name).setLast();
	}
	
	public static boolean exist(String name) {
		return getCooldown(name) != null;
	}
	
	private String name;
	private double last;
	private double duration;
	
	public Cooldown(String name, long duration) {
		this.last = Helper.getTime();
		this.name = name;
		this.duration = duration;
	}

	public String getName() {
		return name;
	}
	
	public double getDuration() {
		return duration;
	}
	
	public void setLast() {
		this.last = Helper.getTime();
	}
	
	public double getLast() {
		return last;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Cooldown)) {
			return false;
		}
		Cooldown other = (Cooldown) obj;
		return Objects.equals(name, other.name);
	}
	
	
	
}
