package fr.fcgame.client.utils.noise;

public class NoiseOptions {
	public float amplitude;
	public int octaves;
	public float smoothness;
	public float roughness;
	public int offset;	
}

