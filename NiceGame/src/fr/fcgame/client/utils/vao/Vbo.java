package fr.fcgame.client.utils.vao;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashSet;
import java.util.Set;

import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.system.NativeType;

import fr.fcgame.client.utils.lwjgl.Helper;

public class Vbo {

	private int vboID;
	private int bindTarget;
	private int vboAttributeIndex;
	private boolean created;
	private boolean binded;
	
	private static Set<Vbo> loadedVbo = new HashSet<>();
	
	public static boolean existNotClosed() {
		return loadedVbo.stream().filter(Vbo::isBinded).findFirst().isPresent();
	}
	
	public static void unbindAll() {
		for(Vbo vbo : loadedVbo) {
			GL30.glDeleteBuffers(vbo.vboID);
		}
	}
	
	public static Vbo createVbo() {
		return new Vbo();
	}
	
	private Vbo() {
		this.vboID = GL30.glGenBuffers();
		loadedVbo.add(this);
		this.binded = false;
		this.created = false;
	}
	
	public void bind(@NativeType(value="GLenum")  int target) {
		if(!this.binded) {
			this.bindTarget = target;
			if(existNotClosed()) {
				throw new IllegalStateException("Un VBO est déja bind, et vous essayé de bind un nouveau VBO");
			}
			GL30.glBindBuffer(target, this.vboID);
			this.binded = true;
		}
	}
	
	public void storeData(Vao vao, float[] data, int dimensions) {
		if(this.created)return;
		this.vboAttributeIndex = vao.currentVbo++;
		bind(GL15.GL_ARRAY_BUFFER);
		FloatBuffer fb = Helper.toFloatBuffer(data);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, fb, GL15.GL_STATIC_DRAW);
		GL20.glVertexAttribPointer(this.vboAttributeIndex, dimensions, GL20.GL_FLOAT, false, 0, 0);
		unbind();
		this.created = true;
	}
	
	public void storeIndicies(int[] indicies) {
		if(this.created)return;
		bind(GL15.GL_ELEMENT_ARRAY_BUFFER);
		// Le VBO de type index ne doit pas être unbind tant que le vao correspondant n'est pas unbind
		this.binded = false;
		IntBuffer buffer = Helper.toIntBuffer(indicies);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
		this.created = true;
	}
	
	public int getVboID() {
		return vboID;
	}
	
	public void unbind() {
		if(this.binded) {
			GL30.glBindBuffer(this.bindTarget, 0);
			this.binded = false;
		}
	}
	
	public boolean isBinded() {
		return binded;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + vboID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vbo other = (Vbo) obj;
		if (vboID != other.vboID)
			return false;
		return true;
	}
	
}
