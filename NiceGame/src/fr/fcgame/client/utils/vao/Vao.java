package fr.fcgame.client.utils.vao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.lwjgl.opengl.GL30;

public class Vao {

	private int vaoID;
	private boolean binded;
	protected int currentVbo;
	private List<Vbo> bindedVbos;
	
	private static Set<Vao> loadedVao = new HashSet<>();
	
	public static boolean existNotClosed() {
		return loadedVao.stream().filter(Vao::isBinded).findFirst().isPresent();
	}
	
	public static void deleteVAO(Vao v) {
		loadedVao.remove(v);
		v.clearForUpdate();
		System.out.println(v+" deleted.");
		GL30.glDeleteVertexArrays(v.vaoID);
	}
	
	public static void unbindAll() {
		for(Vao vao : loadedVao) {
			GL30.glDeleteVertexArrays(vao.vaoID);
		}
	}
	
	public static Vao createVao() {
		Vao vao = new Vao();
		if(loadedVao.contains(vao)) {
			System.err.println("Error : VAO ID duplication");
			System.exit(1);
		}
		loadedVao.add(vao);
		return vao;
	}
	
	private Vao() {
		this.vaoID = GL30.glGenVertexArrays();
		this.bindedVbos = new ArrayList<>();
		this.binded = false;
	}
	
	public Vao disableVBOs() {
		for(int i = 0; i < this.currentVbo; i++) {
			GL30.glDisableVertexAttribArray(i);
		}
		return this;
	}
	
	public Vao enableVBOs() {
		for(int i = 0; i < this.currentVbo; i++) {
			GL30.glEnableVertexAttribArray(i);
		}
		return this;
	}
	
	public Vao bindData(float[] data, int dimensions) {
		Vbo vbo = Vbo.createVbo();
		vbo.storeData(this, data, dimensions);
		this.bindedVbos.add(vbo);
		return this;
	}
	
	public Vao bindIndices(int[] indices) {
		Vbo vbo = Vbo.createVbo();
		vbo.storeIndicies(indices);
		this.bindedVbos.add(vbo);
		return this;
	}
	
	public Vao bind() {
		if(!this.binded) {
			if(existNotClosed()) {
				throw new IllegalStateException("Une VAO est déja bind, et vous essayé de bind une nouvelle VAO");
			}
			GL30.glBindVertexArray(this.vaoID);
			this.binded = true;
		}
		return this;
	}
	
	public Vao unbind() {
		if(this.binded) {
			GL30.glBindVertexArray(0);
			this.binded = false;
		}
		return this;
	}
	
	public void clearForUpdate() {
		for(Vbo vbo : this.bindedVbos) {
			GL30.glDeleteBuffers(vbo.getVboID());
		}
		this.bindedVbos.clear();
		this.currentVbo = 0;
		this.binded = false;
	}
	
	public boolean isBinded() {
		return binded;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + vaoID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vao other = (Vao) obj;
		if (vaoID != other.vaoID)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Vao [vaoID=" + vaoID + "]";
	}
	
	public int getVaoID() {
		return vaoID;
	}
	
	
	
}
