package fr.fcgame.client.utils.math;

import java.text.DecimalFormat;
import java.util.Random;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import fr.fcgame.client.utils.lwjgl.Camera;
import fr.fcgame.client.utils.lwjgl.Window;

public class MathUtils {

	public static final DecimalFormat FORMAT2F = new DecimalFormat(
				"##.#");

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x le nombre
	 * @param b La base du logarithme
	 * @return
	 * @date 23/12/2019
	 */
	public static int logB(
				int x,
				int b) {
		return (int) (Math.log(x) / Math.log(b));
	}

	/**
	 * Permet de transformer une translation, rotation, et redimension en Matrice
	 * 4x4
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param translation Le vecteur de translation
	 * @param rotation    Le vecteur de rotation
	 * @param scale       la taille (1 étant égal à la taille initiale soit 100%)
	 * @return La Matrice 4x4 créer pour transformer l'entité / l'objet
	 * @date 21/12/2019
	 */
	public static Matrix4f createTransformMatrix(
				Vector3f translation,
				Vector3f rotation,
				float scale) {
		return createTransformMatrix(translation, rotation.x, rotation.y, rotation.z, scale);
	}

	/**
	 * Permet de transformer une translation, rotation, et redimension en Matrice
	 * 4x4
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param translation Le vecteur de translation
	 * @param rx          la rotation selon l'axe X
	 * @param ry          la rotation selon l'axe Y
	 * @param rz          la rotation selon l'axe Z
	 * @param scale       la taille (1 étant égal à la taille initiale soit 100%)
	 * @return La Matrice 4x4 créer pour transformer l'entité / l'objet
	 * @date 21/12/2019
	 */
	public static Matrix4f createTransformMatrix(
				Vector3f translation,
				float rx,
				float ry,
				float rz,
				float scale) {
		return new Matrix4f().translate(translation)
					.rotateXYZ((float) Math.toRadians(rx), (float) Math.toRadians(ry), (float) Math.toRadians(rz))
					.scale(scale);
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param random
	 * @param min
	 * @param max
	 * @return
	 * @date 10/02/2020
	 */
	public static int randomInt(
				Random random,
				int min,
				int max) {
		return random.nextInt((max - min) + 1) + min;
	}

	/**
	 * Effectue le calcul du Vecteur normal pour un triangle donné
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param v0 Le Point 1 du triangle
	 * @param v1 Le Point 2 du triangle
	 * @param v2 Le Point 3 du triangle
	 * @return Le vecteur normal
	 * @date 21/12/2019
	 */
	public static Vector3f calcNormal(
				Vector3f v0,
				Vector3f v1,
				Vector3f v2) {
		Vector3f tangentA = v0.sub(v1, new Vector3f());
		Vector3f tangentB = v0.sub(v2, new Vector3f());
		Vector3f normal = tangentA.cross(tangentB, new Vector3f());
		return normal.normalize();
	}

	/**
	 * Permet de transformer une valeur X d'un intervalle I[m, n] vers une valeur X'
	 * vers un intervalle I'[m', n']
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x       La valeur à mappé
	 * @param in_min  le m de l'intervalle I
	 * @param in_max  le n de l'intervalle I
	 * @param out_min le m' de l'intervalle I'
	 * @param out_max le n' de l'intervalle I'
	 * @return la valeur X' étant dans l'intervalle I'[n', m']
	 * @date 21/12/2019
	 */
	public static float map(
				float x,
				float in_min,
				float in_max,
				float out_min,
				float out_max) {
		return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}

	/**
	 * Permet de mettre une valeur de N dans un intervalle I[n, m]
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x   la valeur
	 * @param min la valeur n de l'intervalle I
	 * @param max la valeur m de l'intervalle I
	 * @return min si x < min <br>
	 *         x si min < x < max <br>
	 *         max si x > max <br>
	 * @date 21/12/2019
	 */
	public static int clamp(
				int x,
				int min,
				int max) {
		if (x < min) {
			x = min;
		} else if (x > max) {
			x = max;
		}
		return x;
	}

	/**
	 * Permet de mettre une valeur de R (double = 64bits) dans un intervalle I[n, m]
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x   la valeur
	 * @param min la valeur n de l'intervalle I
	 * @param max la valeur m de l'intervalle I
	 * @return min si x < min <br>
	 *         x si min < x < max <br>
	 *         max si x > max <br>
	 * @date 21/12/2019
	 */
	public static double clamp(
				double x,
				double min,
				double max) {
		if (x < min) {
			x = min;
		} else if (x > max) {
			x = max;
		}
		return x;
	}

	/**
	 * Permet de mettre une valeur de R (float = 32bits) dans un intervalle I[n, m]
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x   la valeur
	 * @param min la valeur n de l'intervalle I
	 * @param max la valeur m de l'intervalle I
	 * @return min si x < min <br>
	 *         x si min < x < max <br>
	 *         max si x > max <br>
	 * @date 21/12/2019
	 */
	public static float clamp(
				float x,
				float min,
				float max) {
		if (x < min) {
			x = min;
		} else if (x > max) {
			x = max;
		}
		return x;
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x
	 * @return
	 * @date 30/01/2020
	 */
	public static int floor(
				float x) {
		return (int) Math.floor(x);
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x
	 * @return
	 * @date 30/01/2020
	 */
	public static int floor(
				double x) {
		return (int) Math.floor(x);
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x
	 * @return
	 * @date 30/01/2020
	 */
	public static int ceil(
				float x) {
		return (int) Math.ceil(x);
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param x
	 * @return
	 * @date 30/01/2020
	 */
	public static int ceil(
				double x) {
		return (int) Math.ceil(x);
	}

	public static final class Fast {
		private static final int BIG_ENOUGH_INT = 16 * 1024;
		private static final double BIG_ENOUGH_FLOOR = BIG_ENOUGH_INT;
		private static final double BIG_ENOUGH_ROUND = BIG_ENOUGH_INT + 0.5;

		public static int floor(
					float x) {
			return (int) (x + BIG_ENOUGH_FLOOR) - BIG_ENOUGH_INT;
		}

		public static int round(
					float x) {
			return (int) (x + BIG_ENOUGH_ROUND) - BIG_ENOUGH_INT;
		}

		public static int ceil(
					float x) {
			return BIG_ENOUGH_INT - (int) (BIG_ENOUGH_FLOOR - x); // credit: roquen
		}

	}

	private static Matrix4f temp = new Matrix4f();

	public static Matrix4f createOrthoProjection(
				Window window) {
		Matrix4f ortho = new Matrix4f();

		float zNear = 1f;
		float zFar = -1f;
		float m00 = 2f / ((float) window.getWidth());
		float m11 = 2f / ((float) window.getHeight());
		float m22 = -2f / (zFar - zNear);
		float m23 = -(zFar + zNear) / (zFar - zNear);
		float m33 = 1f;

		ortho._m00(m00);
		ortho._m11(m11);
		ortho._m22(m22);
		ortho._m23(m23);
		ortho._m33(m33);
		return ortho;
	}

	/**
	 * Transforme la position, et la vue d'une camera en Matrice 4x4
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param camera
	 * @return La matrice de vue lié à la camera
	 * @date 23/12/2019
	 */
	public static Matrix4f createViewMatrix(
				Camera camera) {
		Vector3f cameraPos = camera.getPosition().add(0, camera.getViewHeight(), 0, new Vector3f());
		Vector3f negativeCameraPos = cameraPos.negate(new Vector3f());
		return temp.identity()
					.rotateX((float) Math.toRadians(camera.getPitch()))
					.rotateY((float) Math.toRadians(camera.getYaw()))
					.translate(negativeCameraPos);
	}

}
