package fr.fcgame.client.utils.game;

import org.joml.Vector3f;

import fr.fcgame.client.components.blocks.Block;
import fr.fcgame.client.utils.lwjgl.model.IndicesMesh;
import fr.fcgame.client.utils.math.MathUtils;
import fr.fcgame.client.utils.vao.Vao;

public class AABB implements Cloneable {

	private Vector3f min;
	private Vector3f max;
	private IndicesMesh mesh;

	public AABB() {
		this(new Vector3f(), new Vector3f());
	}

	public AABB(Vector3f min, Vector3f max) {
		this.min = min;
		this.max = max;
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param center Le centre (les pieds)
	 * @param width  la largeur en X
	 * @param height la hauteur en Y
	 * @param length la profondeur en Z
	 * @date 23/02/2020
	 */
	public AABB(Vector3f center, float width, float height, float length) {
		this.calcFromCenter(center, width/2f, height, length/2f);
	}

	private void calcFromCenter(
				Vector3f center,
				float w2,
				float h2,
				float l2) {
		if (this.min == null) {
			this.min = new Vector3f();
		}
		if (this.max == null) {
			this.max = new Vector3f();
		}

		this.min.set(center.x - w2, center.y, center.z - l2);
		this.max.set(center.x + w2, center.y + h2, center.z + l2);
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param w2 La demi-largeur (width/2) 
	 * @param h La hauteur
	 * @param l2 La demi-profondeur (length/2) 
	 * @date 23/02/2020
	 */
	public void createMesh(
				float w2,
				float h,
				float l2) {
		if (this.mesh != null)
			return;
		IndicesMesh mesh = new IndicesMesh(
					null,
					0);

		int[] indices = {
					// sens inverse des aiguilles d'une montre
					// bottom
					0, 1, 2,
					2, 3, 0,
					// top
					4, 5, 6,
					6, 7, 4,
					// front
					5, 1, 2,
					2, 6, 5,
					// back
					4, 0, 3,
					3, 7, 4,
					// right
					6, 2, 3,
					3, 7, 6,
					// left
					4, 0, 1,
					1, 5, 4
					
		};
		
		float[] vertices = {
					// bottom face
					-w2, 0,  l2, //v0
					-w2, 0, -l2, //v1
					 w2, 0, -l2, //v2
					 w2, 0,  l2, //v3
					// top face
					-w2, h,  l2, //v4
					-w2, h, -l2, //v5
					 w2, h, -l2, //v6
					 w2, h,  l2, //v7
		};
		
		Vao vao = Vao.createVao()
					.bind()
					.bindIndices(indices)
					.bindData(vertices, 3)
					.unbind();
		
		mesh.setVao(vao);
		mesh.setIndicesLength(indices.length);
		this.mesh = mesh;
	}

	public IndicesMesh getMesh() {
		return mesh;
	}
	
	public Vector3f getMin() {
		return min;
	}

	public Vector3f getMax() {
		return max;
	}

	public boolean interset(
				float x,
				float y,
				float z) {
		return (x >= this.min.x && x <= this.max.x) &&
					(y >= this.min.y && y <= this.max.y) &&
					(z >= this.min.z && z <= this.max.z);
	}

	public boolean interset(
				AABB other) {
		return (this.min.x <= other.max.x && this.max.x >= other.min.x) &&
					(this.min.y <= other.max.y && this.max.y >= other.min.y) &&
					(this.min.z <= other.max.z && this.max.z >= other.min.z);
	}

	public boolean interset(
				Block block) {
		return interset(Block.toAABB(block));
	}

	public boolean interset(
				Iterable<Block> blocks) {
		for (Block block : blocks) {
			if (interset(Block.toAABB(block)))
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "AABB [min=" + this.min.toString(MathUtils.FORMAT2F) + ", max=" + this.max.toString(MathUtils.FORMAT2F)
					+ "]";
	}

	@Override
	public AABB clone() {
		AABB aabb = null;
		try {
			aabb = (AABB) super.clone();
		} catch (CloneNotSupportedException e) {
		}
		return aabb;
	}

}
