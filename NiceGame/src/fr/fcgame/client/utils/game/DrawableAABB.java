package fr.fcgame.client.utils.game;

import java.lang.ref.WeakReference;

import org.joml.Vector3f;

import fr.fcgame.client.entity.Entity;

public class DrawableAABB{

	private AABB aabb;
	private WeakReference<Vector3f> worldPosition;
	
	private DrawableAABB(Entity entity) {
		this.worldPosition = new WeakReference<Vector3f>(entity.getPosition());
	}
	
	public WeakReference<Vector3f> getWorldPosition() {
		return worldPosition;
	}
	
	public AABB getAabb() {
		return aabb;
	}
	
	public boolean hasMesh() {
		return this.aabb.getMesh() != null;
	}
	
	public void initMesh() {
		this.aabb.createMesh(1, 1, 1);
	}
	
	public void initMesh(float w, float h, float l) {
		this.aabb.createMesh(w/2f, h, l/2f);
	}
	
}
