package fr.fcgame.client.utils.game;

import org.joml.Vector3f;

public class WorldPosition{

	private float x;
	private float y;
	private float z;

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getZ() {
		return z;
	}

	public void x(float x) {
		this.x = x;
	}
	
	public void y(float y) {
		this.y = y;
	}
	
	public void z(float z) {
		this.z = z;
	}
	
	public Vector3f asVec3() {
		return new Vector3f(x, y, z);
	}

}
