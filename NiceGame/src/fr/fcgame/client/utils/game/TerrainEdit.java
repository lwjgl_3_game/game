package fr.fcgame.client.utils.game;

import org.joml.Vector3i;

import fr.fcgame.client.components.World;
import fr.fcgame.client.components.blocks.Block;
import fr.fcgame.client.components.material.Material;

public final class TerrainEdit {

	private TerrainEdit() {}
	
	/**
	 * Rempli un sphéroïde aplati d'un radius avec un matériel
	 * @author fabien
	 * @date 18/02/2020
	 * @param world le monde
	 * @param centerPos le centre
	 * @param radius le radius du sphéroïde
	 * @param mat le matériel
	 */
	public static void filleOblateSpheroid(World world,
										   Vector3i centerPos,
										   float radius,
										   Material mat) {
		float radiusSquare = radius * radius;
		for (int x = (int) -radius + centerPos.x; x < (int) radius + centerPos.x; x++) {
			for (int z = (int) -radius + centerPos.z; z < (int) radius + centerPos.z; z++) {
				for (int y = (int) -radius + centerPos.y; y < (int) radius + centerPos.y; y++) {
					int dx = x - centerPos.x;
					int dy = y - centerPos.y;
					int dz = z - centerPos.z;
					float f = dx * dx + (2 * dy * dy) + dz * dz;
					if (f < radiusSquare) {
						Block b = world.getBlockAt(x, y, z);
						if(b.getType() != Material.BEDROCK) {
							b.setType(mat);
						}
					}
				}
			}
		}
	}
	
}
