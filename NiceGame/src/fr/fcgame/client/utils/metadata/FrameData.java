package fr.fcgame.client.utils.metadata;

import fr.fcgame.client.utils.lwjgl.Helper;

public class FrameData implements Cloneable{

	private static final FrameData DUMMY_OBJ = new FrameData();
	
	public static FrameData create() {
		FrameData data = DUMMY_OBJ.clone();
		data.start = Helper.getTime();
		return data;
	}
	
	private FrameData() {}
	
	private double start;
	private double duration;
	
	public double getStart() {
		return start;
	}
	
	public double getDuration() {
		return duration;
	}
	
	public void stop() {
		if(this.duration == 0.000D) return;
		this.duration = (Helper.getTime() - start);
	}
	
	@Override
	protected FrameData clone() {
		FrameData frameData = null;
		try {
			frameData = (FrameData) super.clone();
		}catch (Exception e) {}
		return frameData;
	}
	
}
