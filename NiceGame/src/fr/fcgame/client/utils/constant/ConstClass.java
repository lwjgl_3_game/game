package fr.fcgame.client.utils.constant;

import org.joml.Vector3f;

public class ConstClass {

	public static final String FOLDER_DEL_CHAR = "/";

	public static final String RES_FOLDER = "resources"; // Le dossier des resources (type textures, obj)

	/*
	 * 
	 *  Les dossier principaux ou il y a les textures (.jpeg) / models (.obj)
	 * 
	 */
	

	public static final Vector3f X_AXIS = new Vector3f(1, 0, 0);
	public static final Vector3f Y_AXIS = new Vector3f(0, 1, 0);
	public static final Vector3f Z_AXIS = new Vector3f(0, 0, 1);
}
