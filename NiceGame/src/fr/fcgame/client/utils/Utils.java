package fr.fcgame.client.utils;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

import org.joml.Vector3f;
import org.joml.Vector3i;
import org.joml.Vector4f;

import fr.fcgame.client.Game;
import fr.fcgame.client.utils.math.MathUtils;

/**
 * Utilitaire au niveau de la logique du jeu
 * et permet d'assurer le fait qu'une méthode
 * d'OpenGL non Thread-Safe ne soit pas appelé dans
 * un autre Thread que le Thread principal (Ou OpenGL à été initialisé)
 * @author ComminQ_Q (Computer)
 *
 * @date 21/12/2019
 */
public final class Utils {


	public static final Vector3i VEC3I_BUFFER = new Vector3i();
	public static final Vector3i VEC3I_BUFFER1 = new Vector3i();
	public static final Vector3i VEC3I_BUFFER2 = new Vector3i();
	public static final Vector3i VEC3I_BUFFER3 = new Vector3i();
	
	public static final Vector3f VEC3_BUFFER = new Vector3f();
	public static final Vector3f VEC3_BUFFER1 = new Vector3f();
	public static final Vector3f VEC3_BUFFER2 = new Vector3f();
	public static final Vector3f VEC3_BUFFER3 = new Vector3f();

	public static final Vector4f VEC4_BUFFER = new Vector4f();
	public static final Vector4f VEC4_BUFFER1 = new Vector4f();
	public static final Vector4f VEC4_BUFFER2 = new Vector4f();
	public static final Vector4f VEC4_BUFFER3 = new Vector4f();
	
	private Utils() {};
	
	/**
	 * Vérifie si le thread courrant est bien le thread principal
	 * @author ComminQ_Q (Computer)
	 *
	 * @return
	 * @date 21/12/2019
	 */
	public static boolean isMainThread() {
		return Thread.currentThread() != Game.getGame().getMainThread();
	}
	
	/**
	 * Affiche la trace du stack du Thread actuel
	 * @author ComminQ_Q (Computer)
	 *
	 * @date 14/01/2020
	 */
	public static void printStackTrace() {
		StackTraceElement[] elements = Thread.currentThread().getStackTrace();
		for(StackTraceElement element : elements) {
			System.out.println(element.toString()+"\n");
		}
	}
	
	public static CompletableFuture<File> loadFileAsync(String path){
		return CompletableFuture.supplyAsync(() -> new File(path));
	}
	
	public static CompletableFuture<FileInputStream> loadFileInputStreamAsync(String path){
		return CompletableFuture.supplyAsync(() -> {
			try {
				return new FileInputStream(new File(path));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			return null;
		});
	}
	
	public static Vector4f rgbaToVec4f(Color color) {
		VEC4_BUFFER.set(
					Hidden.color_f.apply(color.getRed()),
					Hidden.color_f.apply(color.getGreen()),
					Hidden.color_f.apply(color.getBlue()),
					Hidden.color_f.apply(color.getAlpha()));
		return VEC4_BUFFER;
	}
	
	public static Vector3f rgbToVec3f(Color color) {
		VEC3_BUFFER.set(Hidden.color_f.apply(color.getRed()),
						Hidden.color_f.apply(color.getGreen()),
						Hidden.color_f.apply(color.getBlue()));
		return VEC3_BUFFER;
	}
	
	public static Vector3i toVector3i(Vector3f vec) {
		VEC3I_BUFFER.set(Math.round(vec.x),
						 Math.round(vec.y),
						 Math.round(vec.z));
		return VEC3I_BUFFER;
	}
	
	/**
	 * Ferme le programme s'il y a une tâche asynchrone inapte 
	 * @author ComminQ_Q (Computer)
	 *
	 * @return
	 * @date 21/12/2019
	 */
	public static void catchAsync() {
		if(!isMainThread()) {
			System.err.println("Appel d'un Thread différend du Thread Principal");
			printStackTrace();
			System.exit(1);
		}
	}
	
	private static final class Hidden{
		
		private static final Function<Integer, Float>
				color_f = (scalar) -> MathUtils.map(scalar, 0, 255, 0f, 1.0f);
		
	}
	
}
