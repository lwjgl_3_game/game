package fr.fcgame.client.utils.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Représente un lecteur de fichier
 * @author ComminQ_Q (Computer)
 * 
 * @date 28/11/2019
 */
public class FileReader {

	private String filePath;
	private File file;
	
	/**
	 * Constructeur
	 * @author ComminQ_Q (Computer)
	 *
	 * @param filepath
	 * @date 28/11/2019
	 */
	public FileReader(String filepath) {
		this.filePath = filepath;
	}
	
	/**
	 * 
	 * @author fabien
	 * @date 05/02/2020
	 * @param file
	 */
	public FileReader(File file) {
		this.file = file;
	}
	
	/**
	 * Retourne le fichier du disque sous forme de {@link InputStream}
	 * @author ComminQ_Q (Computer)
	 *
	 * @return
	 * @throws FileNotFoundException
	 * @date 23/12/2019
	 */
	public InputStream asInputStream() throws FileNotFoundException {
		if(this.file != null) {
			return new FileInputStream(this.file);
		}
		return new FileInputStream(new File(this.filePath));
	}
	
	/**
	 * Retourne le fichier du jar sous forme de {@link InputStream}
	 * @author ComminQ_Q (Computer)
	 *
	 * @return
	 * @date 23/12/2019
	 */
	public InputStream asInputStreamFromJar() {
		return Class.class.getResourceAsStream(filePath);
	}
	
	/**
	 * Lie le fichier
	 * @author ComminQ_Q (Computer)
	 *
	 * @return Le stream à lire
	 * @throws FileNotFoundException 
	 * @date 28/11/2019
	 */
	public BufferedReader read() throws FileNotFoundException {
		InputStream stream = new FileInputStream(new File(this.filePath));
		InputStreamReader reader = new InputStreamReader(stream);
		BufferedReader bufferedReader = new BufferedReader(reader);
		return bufferedReader;
	}
	
	
	/**
	 * Lie le fichier
	 * @author ComminQ_Q (Computer)
	 *
	 * @return Le stream à lire
	 * @date 28/11/2019
	 */
	public BufferedReader readFromJar() {
		InputStream stream = Class.class.getResourceAsStream(filePath);
		InputStreamReader reader = new InputStreamReader(stream);
		BufferedReader bufferedReader = new BufferedReader(reader);
		return bufferedReader;
	}
	
}
