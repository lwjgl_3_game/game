package fr.fcgame.client.utils.lwjgl.texture;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.EXTTextureFilterAnisotropic;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL30;

import fr.fcgame.client.utils.file.FileReader;
import fr.fcgame.client.utils.file.PNGDecoder;
import fr.fcgame.client.utils.lwjgl.Helper;

public class Texture {

	private static List<Texture> textures = new ArrayList<>();

	public static void clear() {
		for (Texture texture : textures) {
			GL30.glDeleteTextures(texture.getTextureID());
		}
	}

	public static Texture findTextureByPath(String path) {
		return textures.stream().filter(t -> t.getPath().equals(path)).findFirst().orElse(null);
	}	
	
	public static Texture createTextureAndAsign(String path) throws IOException {
		Texture texture = findTextureByPath(path);
		if (texture != null)
			return texture;
		texture = createTexture(path);
		int textureID = GL11.glGenTextures();
		GL13.glActiveTexture(textureID);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureID);
		int foldedWidth = Helper.get2Fold(texture.width);
		int foldedHeight = Helper.get2Fold(texture.height);
		if (isJpg(path)) {
			GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB8, foldedWidth, foldedHeight, 0, GL11.GL_RGB,
					GL11.GL_UNSIGNED_BYTE, texture.buffer);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
		} else {
			GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, foldedWidth, foldedHeight, 0, GL11.GL_RGBA,
					GL11.GL_UNSIGNED_BYTE, texture.buffer);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
		}
		GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
		if(GLFW.glfwExtensionSupported("GL_EXT_texture_filter_anisotropic")) {
			//float amount = Math.max(4f, GL11.glGetFloat(EXTTextureFilterAnisotropic.GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT));
			//GL11.glTexParameterf(GL11.GL_TEXTURE_2D, EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);
		}else {
			System.err.println("Anisotropic not supported");
		}
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST_MIPMAP_NEAREST);
		GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL14.GL_TEXTURE_LOD_BIAS, -0.4f);
		
		texture.setTextureID(textureID);
		GL13.glActiveTexture(0);
		return texture;
	}

	public static Texture createTexture(String path) throws IOException {
		Texture texture = findTextureByPath(path);
		if (texture != null)
			return texture;
		return new Texture(path, isJpg(path) ? PNGDecoder.Format.RGB : PNGDecoder.Format.RGBA);
	}

	private static boolean isJpg(String fileName) {
		int lastIndexOf = fileName.lastIndexOf(".");
		if (lastIndexOf == -1) {
			return false; // empty extension
		}
		return fileName.substring(lastIndexOf).equals("jpg") ? true : false;
	}

	private String path;
	private ByteBuffer buffer;
	private int width;
	private int height;
	private int textureID;

	private Texture(File file, PNGDecoder.Format format) throws IOException {
		this.path = file.getPath();
		InputStream stream = new FileReader(file).asInputStream();
		PNGDecoder decoder = new PNGDecoder(stream);
		this.width = decoder.getWidth();
		this.height = decoder.getHeight();
		ByteBuffer buffer = ByteBuffer.allocateDirect(format.getNumComponents() * this.width * this.height);
		decoder.decode(buffer, this.width * format.getNumComponents(), format);
		buffer.flip();
		stream.close();
		this.buffer = buffer;
		this.textureID = -1;
	}
	
	private Texture(String path, PNGDecoder.Format format) throws IOException {
		this.path = path;
		InputStream stream = new FileReader(path).asInputStream();
		PNGDecoder decoder = new PNGDecoder(stream);
		this.width = decoder.getWidth();
		this.height = decoder.getHeight();
		ByteBuffer buffer = ByteBuffer.allocateDirect(format.getNumComponents() * this.width * this.height);
		decoder.decode(buffer, this.width * format.getNumComponents(), format);
		buffer.flip();
		stream.close();
		this.buffer = buffer;
		this.textureID = -1;
	}

	private Texture(ByteBuffer buffer, int width, int height) {
		this.path = "Atlas";
		this.width = width;
		this.height = height;
		this.buffer = buffer;
		this.textureID = -1;
	}
	
	private Texture(String path) throws IOException {
		this(path, PNGDecoder.Format.RGBA);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Texture other = (Texture) obj;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		return true;
	}

	public boolean isSet() {
		return textureID != -1;
	}

	public int getTextureID() {
		return textureID;
	}

	public void setTextureID(int textureID) {
		if (!isSet()) {
			this.textureID = textureID;
			textures.add(this);
		}
	}

	@Override
	public String toString() {
		return "Texture [path=" + path + ", width=" + width + ", height=" + height + ", textureID=" + textureID + "]";
	}

	public String getPath() {
		return path;
	}

	public ByteBuffer getBuffer() {
		return buffer;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

}
