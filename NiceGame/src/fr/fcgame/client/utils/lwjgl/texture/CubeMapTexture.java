package fr.fcgame.client.utils.lwjgl.texture;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL30;

import fr.fcgame.client.utils.file.PNGDecoder;
import fr.fcgame.client.utils.lwjgl.Helper;

public class CubeMapTexture {

	private static List<CubeMapTexture> textures = new ArrayList<>();

	public static void clear() {
		for (CubeMapTexture texture : textures) {
			GL30.glDeleteTextures(texture.getTextureID());
		}
	}
	
	public static CubeMapTexture createCubeMapAndAsign(String[] path) throws IOException{
		CubeMapTexture texture = new CubeMapTexture();
		int textureID = GL11.glGenTextures();
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, textureID);
		for(int i = 0; i < path.length; i++) {
			TextureData data = loadImage("resources/textures/skybox/"+path[i]+".png");
			GL11.glTexImage2D(
					GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X+i,
					0,
					GL11.GL_RGBA8,
					Helper.get2Fold(data.getWidth()),
					Helper.get2Fold(data.getHeight()),
					0,
					GL11.GL_RGBA,
					GL11.GL_UNSIGNED_BYTE,
					data.getBuffer());
		}
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
		GL13.glActiveTexture(0);
		texture.setTextureID(textureID);
		return texture;
	}

	private static TextureData loadImage(String path) throws IOException{
		FileInputStream fis = new FileInputStream(path);
		PNGDecoder decod = new PNGDecoder(fis);
		int width = decod.getWidth();
		int height = decod.getHeight();
		ByteBuffer buffer = ByteBuffer.allocateDirect(4 * width * height);
		decod.decode(buffer, width * 4, PNGDecoder.Format.RGBA);
		buffer.flip();
		fis.close();
		return new TextureData(width, height, buffer);
	}
	
	private int textureID;

	private CubeMapTexture() {
		// TODO Auto-generated constructor stub
	}
	
	private void setTextureID(int textureID) {
		this.textureID = textureID;
	}
	
	public int getTextureID() {
		return textureID;
	}
	
	
}
