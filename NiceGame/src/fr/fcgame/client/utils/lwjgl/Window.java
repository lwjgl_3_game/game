package fr.fcgame.client.utils.lwjgl;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MAJOR;
import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MINOR;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR_HIDDEN;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR_NORMAL;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_CORE_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_FORWARD_COMPAT;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateCursor;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwSetCursor;
import static org.lwjgl.glfw.GLFW.glfwSetCursorPos;
import static org.lwjgl.glfw.GLFW.glfwSetInputMode;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowIcon;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowTitle;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.awt.Color;
import java.io.IOException;
import java.nio.IntBuffer;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWImage;
import org.lwjgl.glfw.GLFWKeyCallbackI;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.system.MemoryStack;

import fr.fcgame.client.utils.Utils;
import fr.fcgame.client.utils.java.LimitedQueue;
import fr.fcgame.client.utils.lwjgl.texture.Texture;
import fr.fcgame.client.utils.math.MathUtils;
import fr.fcgame.client.utils.metadata.FrameData;

public class Window {

	public static Window currentWindow = null;
	public static GLFWKeyCallbackI keyCallBack;

	public static void setKeyCallBack(GLFWKeyCallbackI callBack) {
		if (currentWindow == null)
			return;
		glfwSetKeyCallback(currentWindow.window, callBack);
		keyCallBack = callBack;
	}

	public static GLFWKeyCallbackI getKeyCallback() {
		return keyCallBack;
	}

	public static Window initWindow(String title, int width, int height) {
		if (currentWindow != null)
			return currentWindow;
		Window window = new Window(title, width, height);
		return currentWindow = window;
	}

	public static Window getWindow() {
		if (currentWindow != null)
			return currentWindow;
		throw new IllegalStateException("La fenêtre n'a pas été activé");
	}

	private long window;
	private int width;
	private int height;
	private boolean vsync;
	private Monitor mainMonitor;
	private GLCapabilities glCaps;
	private int fps;
	private Map<String, Long> customCursors;
	private boolean cursorHidden;
	private double delta;
	private double lastFrameTime;

	public Window(String title, int width, int height) {
		this.width = width;
		this.height = height;
		this.vsync = false;

		/* Création d'une fausse fenêtre pour récup le support OpenGL actuel */
		init();

		long tempWindow = glfwCreateWindow(1, 1, "", NULL, NULL);
		glfwMakeContextCurrent(tempWindow);

		GL.createCapabilities();
		GLCapabilities caps = GL.getCapabilities();

		glfwDestroyWindow(tempWindow);

		/* Suppression de la fenêtre fictive */
		glfwDefaultWindowHints();
		if (caps.OpenGL40) {
			/* OpenGL 4.0 core profile */
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
			glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
			System.out.println("OpenGL 4.0 enabled");
		} else if (caps.OpenGL32) {
			/* legacy OpenGL 3.2 */
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
			glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
			System.out.println("OpenGL 3.2 enabled");
		} else {
			System.err.println("Aucun des profil OpenGL n'est supporté (OpenGL 4.0, OpenGL 3.2), mettez à jour vos drivers");
			System.exit(-1);
			return;
		}
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

		/* Création de la fenêtre avec le contexte OpenGL adapté */
		this.window = glfwCreateWindow(width, height, title, NULL, NULL);
		if (this.window == NULL) {
			glfwTerminate();
			throw new RuntimeException("Failed to create the GLFW window!");
		}

		/* Centrer l'écran sur le moniteur numéro 1 */
		this.mainMonitor = new Monitor(GLFW.glfwGetPrimaryMonitor());
		this.center();

		/* Création du contexte OpenGL */
		glfwMakeContextCurrent(this.window);

		glfwShowWindow(this.window);

		this.glCaps = GL.createCapabilities();

		
		// On désactive le rendu d'une face qu'on ne voit pas
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glCullFace(GL11.GL_BACK);
		GL11.glEnable( GL11.GL_NORMALIZE);
		this.lastTime = Helper.getTimeInSeconds();
		
		Inputs.init(this.window);
	}

	public void setCursorPosition(double pixelX, double pixelY) {
		glfwSetCursorPos(this.getWindowID(), pixelX, pixelY);
	}

	public void hideCursor() {
		glfwSetInputMode(this.getWindowID(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	}

	public void showCursor() {
		glfwSetInputMode(this.getWindowID(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}

	/**
	 * Cette fonction doit être appelé dans le Thread principal
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param texture La Texture du curseur
	 * @date 06/12/2019
	 */
	public void setCustomCursor(Texture texture) {
		// On check si c'est bien le thread principal
		Utils.catchAsync();

		// Si on à déja enregisstatictré cette texture, alors on ré-utilise
		if (customCursors.containsKey(texture.getPath())) {
			// On récup l'ID du curseur
			long cursorID = customCursors.get(texture.getPath());

			// On affecte le curseur pour la fenêtre principale
			glfwSetCursor(Window.getWindow().getWindowID(), cursorID);
			return;
		}
		// On créer une image GLFW
		GLFWImage image = GLFWImage.create();
		// On affecte les données de l'objet texture à l'objet de l'image GLFW
		image.set(texture.getWidth(), texture.getHeight(), texture.getBuffer());

		/* Code externe */
		// the hotspot indicates the displacement of the sprite to the
		// position where mouse clicks are registered
		int hotspotX = 3;
		int hotspotY = 6;

		// On créer le curseur et on récup le pointeur du curseur
		long cursorID = glfwCreateCursor(image, hotspotX, hotspotY);

		// On Ajoute le curseur à la liste des curseurs créé
		this.customCursors.put(texture.getPath(), cursorID);

		// On affecte le curseur à la fenêtre principale
		glfwSetCursor(this.getWindowID(), cursorID);
		this.lastFrameTime = Helper.getTime();
	}

	private void init() {
		if (!glfwInit())
			throw new IllegalStateException("Unable to initialize GLFW");

		// Configure GLFW
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

		// Compatibilité pour le Linux
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	}

	public void show() {
		GLFW.glfwShowWindow(this.window);
	}

	public void hide() {
		GLFW.glfwHideWindow(this.window);
	}

	public void setClearColor(Color color) {
		float r = color.getRed(), g = color.getGreen(), b = color.getBlue(), a = color.getAlpha();

		// On mape les valeurs de 0-255 (Entier) sur 0-1 (Réel)
		r = MathUtils.map(r, 0, 255, 0, 1);
		g = MathUtils.map(g, 0, 255, 0, 1);
		b = MathUtils.map(b, 0, 255, 0, 1);
		a = MathUtils.map(a, 0, 255, 0, 1);

		glClearColor(r, g, b, a);
	}

	public void updateWidthAndHeight() {
		try (MemoryStack stack = stackPush()) {
			IntBuffer width = stack.mallocInt(1);
			IntBuffer height = stack.mallocInt(1);

			GLFW.glfwGetWindowSize(this.window, width, height);

			this.width = width.get(0);
			this.height = width.get(0);
		}

	}

	public int getFps() {
		return fps;
	}

	private int tempFps;
	private double lastTime;
	private final double accuracy = 1.00000;

	private void updateFps() {
		double currentTime = Helper.getTimeInSeconds();
		this.tempFps++;
		if(currentTime - this.lastTime >= this.accuracy) {
			this.fps = tempFps;
			System.out.println(String.format("%f ms / images", 1000.0/Double.valueOf(tempFps)));
			this.tempFps = 0;
			this.lastTime+=this.accuracy;
		}
	}

	public void pollEvents() {
		this.updateFps();
		GLFW.glfwPollEvents();
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT|GL11.GL_DEPTH_BUFFER_BIT);
	}

	private LimitedQueue<Double> frameQueue = new LimitedQueue<Double>(50);
	
	public void swapBuffers() {
		double currentFrameTime = Helper.getTime();
		this.delta = (currentFrameTime - this.lastFrameTime);
		GLFW.glfwSwapBuffers(this.window);
		this.lastFrameTime = currentFrameTime;
		this.frameQueue.add(this.delta);
	}
	
	public void close() {
		glfwFreeCallbacks(this.window);
		glfwDestroyWindow(this.window);
	}
	
	public double getDelta() {
		return delta;
	}

	public void center() {
		glfwSetWindowPos(window, (this.mainMonitor.getWidth() - this.getWidth()) / 2,
				(this.mainMonitor.getHeight() - this.getHeight()) / 2);
	}

	public boolean hasToBeClosed() {
		return GLFW.glfwWindowShouldClose(this.window);
	}

	public void setWindowName(String name) {
		glfwSetWindowTitle(this.window, name);
	}

	public void setWindowIcon(String path) throws IOException {
		Texture texture = Texture.createTexture(path);
		GLFWImage.Buffer imageBuffer = new GLFWImage.Buffer(texture.getBuffer());
		glfwSetWindowIcon(this.window, imageBuffer);
	}

	public float getAspectRatio() {
		return Float.valueOf(this.width) / Float.valueOf(this.height);
	}

	public boolean isVsyncEnabled() {
		return vsync;
	}

	/**
	 * 
	 * Active ou désactive la synchro verticale
	 *
	 * @param vsync
	 */
	public void setVSync(boolean vsync) {
		this.vsync = vsync;
		if (vsync) {
			glfwSwapInterval(1);
		} else {
			glfwSwapInterval(0);
		}
	}
	
	public void updateCursors() {
		if(this.cursorHidden) {
			this.showCursor();
			this.cursorHidden = false;
		}else {
			this.hideCursor();
			this.cursorHidden = true;
		}
	}

	public long getWindowID() {
		return window;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public Monitor getMainMonitor() {
		return mainMonitor;
	}

	public boolean isCursorHidden() {
		return cursorHidden;
	}

	public void setCursorHidden(boolean cursorHidden) {
		this.cursorHidden = cursorHidden;
	}

}
