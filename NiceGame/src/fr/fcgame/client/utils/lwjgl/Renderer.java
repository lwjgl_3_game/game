package fr.fcgame.client.utils.lwjgl;

import org.lwjgl.opengl.GL11;

import fr.fcgame.client.Game;
import fr.fcgame.client.utils.lwjgl.shaders.ShaderProgram;

public abstract class Renderer<T> {

	public static final int PRIORITY_LOWEST = 0;
	public static final int PRIORITY_LOW = 1;
	public static final int PRIORITY_NORMAL = 2;
	public static final int PRIORITY_HIGH = 3;
	public static final int PRIORITY_HIGHEST = 4;
	public static final int PRIORITY_MONITOR = 5;
	
	private boolean wireframe;
	private int priority;
	protected double lastDisplayInformation;
	private Class<? extends ShaderProgram> linkedShader;
	
	public Renderer() {
		this.wireframe = false;
	}

	protected abstract void injectMainUniforms(ShaderProgram shaderProgram);
	
	protected abstract void injectForEachUniforms(T type, ShaderProgram shaderProgram);
	
	protected abstract void render(Game game, Window window, ShaderProgram shaderProgram);
	
	public abstract void init(Game game, Window window);
	
	public void renderScene(ShaderProgram shaderProgram) {
		this.pre();
		this.render(Game.getGame(), Window.getWindow(), shaderProgram);
		this.post();
	}

	protected void pre() {
		if(this.wireframe) {
        	GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
        }
	}
	
	protected void post() {
		if(this.wireframe) {
        	GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
        }
	}
	
	public void setWireframe(boolean wireframe) {
		this.wireframe = wireframe;
	}
	
	public boolean isWireframe() {
		return wireframe;
	}
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}
	
	@Override
	public int hashCode() {
		return Renderer.class.getSimpleName().hashCode();
	}

	public int getPriority() {
		return priority;
	}

	protected void setPriority(int priority) {
		this.priority = priority;
	}

	public Class<? extends ShaderProgram> getLinkedShader() {
		return linkedShader;
	}

	public void setLinkedShader(Class<? extends ShaderProgram> linkedShader) {
		this.linkedShader = linkedShader;
	}
	
}

