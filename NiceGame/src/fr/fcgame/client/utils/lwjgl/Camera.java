package fr.fcgame.client.utils.lwjgl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector3i;

import fr.fcgame.client.Game;
import fr.fcgame.client.components.Chunk;
import fr.fcgame.client.components.World;
import fr.fcgame.client.components.blocks.Block;
import fr.fcgame.client.components.material.Material;
import fr.fcgame.client.entity.EntityLiving;
import fr.fcgame.client.entity.EntityPlayer;
import fr.fcgame.client.events.EventsManager;
import fr.fcgame.client.events.world.block.BlockBreakEvent;
import fr.fcgame.client.state.Controller;
import fr.fcgame.client.utils.Utils;
import fr.fcgame.client.utils.java.Triplet;
import fr.fcgame.client.utils.math.MathUtils;

public class Camera {

	public static final float PLAYER_HEIGHT = 2f;
	public static final float PLAYER_WIDTH = 1f;

	public static final float NEAR_PLANE = 0.001f;
	public static final float FAR_PLANE = 1000;

	private static final float SCALAR_SPEED = 0.01f;
	private static final float SPEED = 8f;
	private static final int RANGE = 6;
	public static float MOUSE_SENS = 0.1f;

	private int fov;
	private Matrix4f projectionMatrix;
	private Block selectedBlock;
	private EntityPlayer entityPlayer;
	private List<Block> blocks;
	private Game game;

	public Camera(Game game, EntityPlayer mainPlayer) {
		this.fov = 95;
		mainPlayer.setPosition(new Vector3f(
					0,
					World.CHUNK_HEIGHT + 5,
					0));
		mainPlayer.setRotation(0, 0);

		this.projectionMatrix = new Matrix4f();
		this.entityPlayer = mainPlayer;
		updateProjectionMatrix();
		this.blocks = new ArrayList<>();
		this.game = game;
	}

	public void setEntityPlayer(
				EntityPlayer entityPlayer) {
		this.entityPlayer = entityPlayer;
	}

	public void updateProjectionMatrix() {
		float aspectRatio = Window.getWindow().getAspectRatio();
		float y_scale = (float) ((1f / Math.tan(Math.toRadians(this.fov / 2f))) * aspectRatio);
		float x_scale = y_scale / aspectRatio;
		float frustum_length = FAR_PLANE - NEAR_PLANE;

		this.projectionMatrix._m00(x_scale);
		this.projectionMatrix._m11(y_scale);
		this.projectionMatrix._m22(-((FAR_PLANE + NEAR_PLANE) / frustum_length));
		this.projectionMatrix._m23(-1);
		this.projectionMatrix._m32(-((2 * NEAR_PLANE * FAR_PLANE) / frustum_length));
		this.projectionMatrix._m33(0);
	}

	public Matrix4f getProjectionMatrix() {
		return projectionMatrix;
	}

	public Vector3f getPosition() {
		return new Vector3f(
					this.entityPlayer.getPosition());
	}

	public Vector3f getDirectionVector() {
		double pitchRadians = Math.toRadians(-this.entityPlayer.getRotation().x);
		double yawRadians = Math.toRadians(-this.entityPlayer.getRotation().y);

		float sinPitch = (float) Math.sin(pitchRadians);
		float cosPitch = (float) Math.cos(pitchRadians);
		float sinYaw = (float) Math.sin(yawRadians);
		float cosYaw = (float) Math.cos(yawRadians);

		return new Vector3f(
					-cosPitch * sinYaw,
					sinPitch,
					-cosPitch * cosYaw);
	}

	public Ray createRay() {
		return new Ray(
					getDirectionVector(),
					getPosition());
	}

	public void moveMouse() {
		if (Game.getGame().getCurrentController() == Controller.GAME) {
			Triplet<Double, Double, Boolean> delta = Inputs.getMouseMovementDelta();
			if (delta != null) {
				double deltaX = delta.getT1();
				double deltaY = delta.getT2();

				float relYaw = (float) deltaX * MOUSE_SENS;
				float relPitch = (float) deltaY * MOUSE_SENS;

				this.entityPlayer.getRotation().y += relYaw;
				this.entityPlayer.getRotation().x += relPitch;

				this.entityPlayer.getRotation().y %= 360;
				this.entityPlayer.getRotation().x %= 360;
				this.entityPlayer.getRotation().x = MathUtils.clamp(this.entityPlayer.getRotation().x, -90, 90);
			}
		}
	}

	DecimalFormat d = new DecimalFormat(
				"##,##");
	int n = 0;

	public void mouse() {
		if (Inputs.mouseButtonDown(org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_RIGHT)) {
			Ray ray = this.createRay();
			for (; ray.length() < RANGE; ray.step()) {
				if (this.game.getWorld().getBlockTypeAt(ray.toBlockPostion()) != Material.AIR) {
					Block b = Game.getGame().getWorld().getBlockAt(ray.toBlockPostion());
					BlockBreakEvent event = new BlockBreakEvent(
								b);
					EventsManager.callEvent(event);
					if (event.isCancelled()) {
						System.out.println("Event has been canceled");
						break;
					}
					b.setType(Material.AIR);
					this.game.getWorld().addChunkUpdate(b.getChunk());
					checkForSurroundingChunks(b.getBlockPosition());
					break;
				}
			}
		}
	}

	public void checkForSurroundingChunks(
				Vector3i block) {
		World world = Game.getGame().getWorld();
		Chunk c = world.getChunkAtBlock(block.x, block.z);
		Vector3i relative = world.getRelativeCoords(block);
		if (relative.x % World.CHUNK_SIZE == 0) {
			updateChunk(c.getX() - 1, c.getZ(), world);
		}
		if (relative.x % World.CHUNK_SIZE == World.CHUNK_SIZE - 1) {
			updateChunk(c.getX() + 1, c.getZ(), world);
		}
		if (relative.z % World.CHUNK_SIZE == 0) {
			updateChunk(c.getX(), c.getZ() - 1, world);
		}
		if (relative.z % World.CHUNK_SIZE == World.CHUNK_SIZE - 1) {
			updateChunk(c.getX(), c.getZ() + 1, world);
		}

	}

	private void updateChunk(
				int x,
				int z,
				World world) {
		Chunk c = world.getChunkAt(x, z);
		world.addChunkUpdate(c);
	}

	public void move() {
		if (this.game.getCurrentController() == Controller.GAME) {
			float fSpeed = (float) (SPEED * this.game.getDelta() * SCALAR_SPEED);
			float translateX = 0f;
			float translateY = 0f;
			float translateZ = 0f;
			float yaw = this.entityPlayer.getRotation().y;

			if (Inputs.keyDown(org.lwjgl.glfw.GLFW.GLFW_KEY_W)) {
				float rotY = -(yaw % 360);
				translateX -= (float) (fSpeed * Math.sin(Math.toRadians(rotY)));
				translateZ -= (float) (fSpeed * Math.cos(Math.toRadians(rotY)));

			}
			if (Inputs.keyDown(org.lwjgl.glfw.GLFW.GLFW_KEY_S)) {
				float rotY = -(yaw % 360);
				translateX += (float) (fSpeed * Math.sin(Math.toRadians(rotY)));
				translateZ += (float) (fSpeed * Math.cos(Math.toRadians(rotY)));
			}

			if (Inputs.keyDown(org.lwjgl.glfw.GLFW.GLFW_KEY_D)) {
				float rotY = -(yaw - 90 % 360);
				translateX += (float) (fSpeed * Math.sin(Math.toRadians(rotY)));
				translateZ += (float) (fSpeed * Math.cos(Math.toRadians(rotY)));
			}

			if (Inputs.keyDown(org.lwjgl.glfw.GLFW.GLFW_KEY_A)) {
				float rotY = -(yaw + 90 % 360);
				translateX += (float) (fSpeed * Math.sin(Math.toRadians(rotY)));
				translateZ += (float) (fSpeed * Math.cos(Math.toRadians(rotY)));
			}

			if (Inputs.keyDown(org.lwjgl.glfw.GLFW.GLFW_KEY_SPACE)) {
				translateY += fSpeed;
			}

			if (Inputs.keyDown(org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT_SHIFT)) {
				translateY -= fSpeed;
			}

			Vector3f oldPos = Utils.VEC3_BUFFER2.set(this.entityPlayer.getPosition());

			this.entityPlayer.translate(translateX, translateY, translateZ);
			Vector3i voxelPos = Helper.toVoxelPos(this.entityPlayer.getPosition(), Utils.VEC3I_BUFFER).add(0, -1, 0);
			if (this.game.getWorld().getBlockTypeAt(voxelPos).isOpaque()) {
				this.entityPlayer.setPosition(oldPos);
			}
			voxelPos = Helper.toVoxelPos(this.entityPlayer.getEyePosition(Utils.VEC3_BUFFER3), Utils.VEC3I_BUFFER)
						.add(0, -1, 0);
			if (this.game.getWorld().getBlockTypeAt(voxelPos).isOpaque()) {
				this.entityPlayer.setPosition(oldPos);
			}

			n++;
			if (n > 900) {
				System.out.println("Camera Coord : " + this.getPositionFormated());
				n = 0;
			}
			this.blocks.clear();
		}

	}

	public String getPositionFormated() {
		Chunk chunk = this.game.getWorld().getChunkAtBlock(this.entityPlayer.getPosition().x,
					this.entityPlayer.getPosition().z);
		String chunkPosition = "NotInChunk";
		if (chunk != null) {
			chunkPosition = chunk.getPosition().toString(MathUtils.FORMAT2F);
		}
		return "Position :" + this.entityPlayer.getPosition().toString(MathUtils.FORMAT2F)
					+ ", Chunk Position : " + chunkPosition
					+ ", Relative Position : "
					+ this.game.getWorld().getRelativeCoords(Utils.toVector3i(this.entityPlayer.getPosition()))
								.toString(MathUtils.FORMAT2F);
	}

	public float getPitch() {
		return this.entityPlayer.getRotation().x;
	}

	public float getYaw() {
		return this.entityPlayer.getRotation().y;
	}

	public Block getSelectedBlock() {
		return selectedBlock;
	}

	public void setSelectedBlock(
				Block selectedBlock) {
		this.selectedBlock = selectedBlock;
	}

	public void processPlayer() {
		((EntityLiving) this.entityPlayer).processVelocity();
		((EntityLiving) this.entityPlayer).processPosition();
	}

	public float getViewHeight() {
		return this.entityPlayer.getEyeLocation();
	}

}
