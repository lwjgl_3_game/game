package fr.fcgame.client.utils.lwjgl;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.joml.Vector3f;
import org.joml.Vector3i;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;

import fr.fcgame.client.utils.Utils;
import fr.fcgame.client.utils.lwjgl.model.Mesh;
import fr.fcgame.client.utils.lwjgl.object.Meshable;
import fr.fcgame.client.utils.math.MathUtils;

public class Helper {
	
	private Helper() {};

	/**
	 * Renvoie le plus petit nombre proche de fold ayant une puissance de 2
	 * @author fabien
	 * @date 31/01/2020
	 * @param fold
	 * @return
	 */
	public static final int get2Fold(int fold) {
        int ret = 2;
        while (ret < fold) {
            ret *= 2;
        }
        return ret;
    }
	
	/**
	 * ThreadSafe, renvoie le temps actuel selon GLFW
	 * @author fabien
	 * @date 08/12/2019
	 * @return le temps en secondes
	 */
	public static double getTimeInSeconds() {
	    return GLFW.glfwGetTime();
	}
	
	/**
	 * ThreadSafe, renvoie le temps actuel selon GLFW
	 * @author fabien
	 * @date 08/12/2019
	 * @return le temps en secondes
	 */
	public static double getTime() {
	    return (double) (GLFW.glfwGetTime()*1000);
	}
	
	/**
	 * 
	 * @author fabien
	 * @date 18/02/2020
	 * @return
	 */
	public static Vector3i toVoxelPos(Vector3f from, Vector3i buffer) {
		buffer.set(
				MathUtils.Fast.round(from.x-0.5f),
				MathUtils.Fast.round(from.y),
				MathUtils.Fast.round(from.z-0.5f)
				);
		return buffer;
	}
	
	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <T>
	 * @param list
	 * @return La Map content chaque Mesh avec la liste <T> lié au mesh
	 * @date 24/12/2019
	 */
	public static <T extends Meshable, M extends Mesh> Map<M, List<T>> filter(List<T> list){
		Map<M, List<T>> map = new HashMap<M, List<T>>();
		for(T type : list) {
			if(map.containsKey(type.getMesh())) {
				map.get(type.getMesh()).add(type);
			}else {
				List<T> ls = new ArrayList<>();
				ls.add(type);
				map.put(type.getMesh(), ls);
			}
		}
		return map;
	}
	
	public static FloatBuffer toFloatBuffer(float[] data) {
		FloatBuffer floatBuffer = BufferUtils.createFloatBuffer(data.length);
		floatBuffer.put(data);
		floatBuffer.flip();
		return floatBuffer;
	}
	
	public static IntBuffer toIntBuffer(int[] data) {
		IntBuffer intBuffer = BufferUtils.createIntBuffer(data.length);
		intBuffer.put(data);
		intBuffer.flip();
		return intBuffer;
	}
	
	
	
}
