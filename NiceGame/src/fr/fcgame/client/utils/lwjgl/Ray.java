package fr.fcgame.client.utils.lwjgl;

import org.joml.Vector3f;
import org.joml.Vector3i;

public class Ray {

	private Vector3f normalize;
	private int currentIteration;
	private Vector3f cameraPosition;
	
	public Ray(Vector3f vec, Vector3f cameraPosition) {
		vec.normalize(this.normalize = new Vector3f());
		this.cameraPosition = cameraPosition;
	}
	
	public Vector3f getVector() {
		return this.normalize;
	}
	
	public float length() {
		return this.normalize.length() * this.currentIteration;
	}
	
	public Vector3i toBlockPostion() {
		Vector3f n = new Vector3f();
		this.normalize.mul(this.currentIteration, n).add(this.cameraPosition, n);
		Vector3i i = new Vector3i((int) Math.floor(n.x), (int) Math.floor(n.y), (int) Math.floor(n.z));
		return i;
	}
	
	public float rawLength() {
		return normalize.length();
	}
	
	public void step() {
		this.currentIteration++;
	}
	
}
