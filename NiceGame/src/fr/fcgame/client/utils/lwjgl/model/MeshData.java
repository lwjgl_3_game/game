package fr.fcgame.client.utils.lwjgl.model;

import java.util.HashMap;
import java.util.Map;

public class MeshData {

	public static final String POSITIONS = "positions";
	public static final String INDICES = "indices";
	
	private Map<String, Object> list;
	
	public MeshData() {
		this.list = new HashMap<String, Object>();
	}
	
	public Map<String, Object> getList() {
		return list;
	}
	
	public void addIntArray(String name, int[] array) {
		this.list.put(name, array);
	}
	
	public void addFloatArray(String name, float[] array) {
		this.list.put(name, array);
	}
	
	public int[] getIntArray(String name) {
		return (int[]) this.list.get(name);
	}
	
	public float[] getFloatArray(String name) {
		return (float[]) this.list.get(name);
	}
	
}
