package fr.fcgame.client.utils.lwjgl.model;

import java.lang.ref.WeakReference;

import fr.fcgame.client.utils.lwjgl.texture.Texture;
import fr.fcgame.client.utils.vao.Vao;

public class TexturedMesh extends IndicesMesh{

	private WeakReference<Texture> texture;
	
	public TexturedMesh(Vao vaoID, Texture texture, int indicesLength) {
		super(vaoID, indicesLength);
		this.texture = new WeakReference<Texture>(texture);
	}

	public WeakReference<Texture> getWeakTexture() {
		return texture;
	}
	
}
