package fr.fcgame.client.utils.lwjgl.model;

import org.lwjgl.opengl.GL11;

import fr.fcgame.client.utils.vao.Vao;

public class VerticesMesh extends Mesh{

	private int verticesCount;

	public VerticesMesh(Vao vaoID, int verticesCount) {
		super(vaoID);
		this.verticesCount = verticesCount;
	}
	
	@Override
	public void drawMesh() {
		GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, this.verticesCount);
	}
	
	
	
}
