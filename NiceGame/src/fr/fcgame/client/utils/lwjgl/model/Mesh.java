package fr.fcgame.client.utils.lwjgl.model;

import org.lwjgl.opengl.GL11;

import fr.fcgame.client.utils.vao.Vao;

public abstract class Mesh {

	protected Vao vao;
	
	public Mesh(Vao vaoID) {
		this.vao = vaoID;
	}
	
	public abstract void drawMesh();
	
	public boolean hasVao() {
		return this.vao != null;
	}
	
	public void setVao(Vao vao) {
		if(hasVao())return;
		this.vao = vao;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vao == null) ? 0 : vao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mesh other = (Mesh) obj;
		if (vao == null) {
			if (other.vao != null)
				return false;
		} else if (!vao.equals(other.vao))
			return false;
		return true;
	}

	public Vao getVao() {
		return vao;
	}

	
	
	
}
