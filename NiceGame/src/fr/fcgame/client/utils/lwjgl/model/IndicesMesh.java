package fr.fcgame.client.utils.lwjgl.model;

import org.lwjgl.opengl.GL11;

import fr.fcgame.client.utils.vao.Vao;

public class IndicesMesh extends Mesh{

	private int indicesLength;
	
	public IndicesMesh(Vao vaoID, int indicesLength) {
		super(vaoID);
		this.indicesLength = indicesLength;
	}
	
	@Override
	public void drawMesh() {
		GL11.glDrawElements(GL11.GL_TRIANGLES, this.indicesLength, GL11.GL_UNSIGNED_INT, 0);
	}
	
	public int getIndicesLength() {
		return indicesLength;
	}
	
	public void setIndicesLength(int indicesLength) {
		this.indicesLength = indicesLength;
	}

}
