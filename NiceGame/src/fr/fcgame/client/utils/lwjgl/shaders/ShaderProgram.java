package fr.fcgame.client.utils.lwjgl.shaders;

import java.io.BufferedReader;
import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL32;

import fr.fcgame.client.utils.file.FileReader;

public abstract class ShaderProgram {

	public static Set<ShaderProgram> shaderPrograms = new HashSet<>();
	
	public static boolean existNonClosed() {
		return shaderPrograms.stream().filter(sp -> sp.open).findFirst().isPresent();
	}
	
	public static Set<ShaderProgram> getShaderPrograms() {
		return shaderPrograms;
	}
	
	private static FloatBuffer MATRIX_BUFFER = BufferUtils.createFloatBuffer(16);

	
	private Map<String, Integer> uniformVariables;
	private int programID;
	
	// Listes des .glsl
	private int vertexShaderID;
	private int fragmentShaderID;
	private int geometryShaderID;
	
	
	private boolean open;

	public ShaderProgram(String vertexFile, String fragmentFile) {
		if(shaderPrograms.contains(this)) {
			return;
		}
		this.uniformVariables = new HashMap<String, Integer>();
		this.vertexShaderID = loadShader(vertexFile, GL20.GL_VERTEX_SHADER);
		this.fragmentShaderID = loadShader(fragmentFile, GL20.GL_FRAGMENT_SHADER);
		this.programID = GL20.glCreateProgram();
		GL20.glAttachShader(this.programID, this.vertexShaderID);
		GL20.glAttachShader(this.programID, this.fragmentShaderID);
		bindAttributes();
		GL20.glLinkProgram(this.programID);
		GL20.glValidateProgram(this.programID);
		getAllUniformLocations();
		
		shaderPrograms.add(this);
		this.geometryShaderID = -1;
	}
	
	public ShaderProgram(String vertexFile, String geometryFile, String fragmentFile) {
		if(shaderPrograms.contains(this)) {
			return;
		}
		this.uniformVariables = new HashMap<String, Integer>();
		this.vertexShaderID = loadShader(vertexFile, GL20.GL_VERTEX_SHADER);
		this.fragmentShaderID = loadShader(fragmentFile, GL20.GL_FRAGMENT_SHADER);
		this.geometryShaderID = loadShader(geometryFile, GL32.GL_GEOMETRY_SHADER);
		this.programID = GL20.glCreateProgram();
		GL20.glAttachShader(this.programID, this.vertexShaderID);
		GL20.glAttachShader(this.programID, this.fragmentShaderID);
		GL20.glAttachShader(this.programID, this.geometryShaderID);
		bindAttributes();
		GL20.glLinkProgram(this.programID);
		GL20.glValidateProgram(this.programID);
		getAllUniformLocations();
		
		shaderPrograms.add(this);
	}

	protected abstract void getAllUniformLocations();

	public int getUniformVariable(String name) {
		return this.uniformVariables.get(name);
	}
	
	public void registerUniform(String name) {
		if(this.uniformVariables.containsKey("name")) {
			return;
		}
		this.uniformVariables.put(name, getUniformLocation(name));
	}
	
	public void injectFloat(int location, float value) {
		GL20.glUniform1f(location, value);
	}

	public void injectFloatArray(int uniformLocation, float[] floatArray) {
		if(floatArray == null) {
			throw new IllegalArgumentException("Le tableau de vecteur est nul");
		}
		GL20.glUniform1fv(uniformLocation, floatArray);
	}
	
	public void injectVector(int location, Vector3f value) {
		GL20.glUniform3f(location, value.x, value.y, value.z);
	}
	
	public void injectVector4f(int location, Vector4f value) {
		GL20.glUniform4f(location, value.x, value.y, value.z, value.w);
	}

	public void injectBoolean(int location, boolean value) {
		injectFloat(location, value ? 1 : 0);
	}
	
	public void injectVectorArray(int uniformLocation, Vector3f[] vectorArray) {
		if(vectorArray == null) {
			throw new IllegalArgumentException("Le tableau de vecteur est nul");
		}
		FloatBuffer vectorBuffer = BufferUtils.createFloatBuffer(vectorArray.length * 3);
		for (int i = 0; i < vectorArray.length; i++) {
			vectorBuffer.put(vectorArray[i].x);
			vectorBuffer.put(vectorArray[i].y);
			vectorBuffer.put(vectorArray[i].z);
		}
		vectorBuffer.flip();
		GL20.glUniform3fv(uniformLocation, vectorBuffer);
	}

	protected void injectMatrix(int location, Matrix4f matrix) {
		GL20.glUniformMatrix4fv(location, false, matrix.get(MATRIX_BUFFER));
	}

	protected int getUniformLocation(String uniformName) {
		return GL20.glGetUniformLocation(this.programID, uniformName);
	}

	/**
	 * Supprime le shader
	 * @author ComminQ_Q (Computer)
	 *
	 * @date 21/12/2019
	 */
	public void cleanUp() {
		stop();
		GL20.glDetachShader(this.programID, this.fragmentShaderID);
		GL20.glDetachShader(this.programID, this.vertexShaderID);
		
		// On check s'il y a un shader de type géométrie associé au programme
		if(this.geometryShaderID != -1) {
			GL20.glDetachShader(this.programID, this.geometryShaderID);
		}
		
		GL20.glDeleteShader(this.fragmentShaderID);
		GL20.glDeleteShader(this.vertexShaderID);

		// On check s'il y a un shader de type géométrie associé au programme
		if(this.geometryShaderID != -1) {
			GL20.glDeleteShader(this.geometryShaderID);
		}
		
		GL20.glDeleteProgram(this.programID);
	}

	public void start() {
		if(existNonClosed()) {
			System.err.println("Error, shader start when non closed exist");
		}
		GL20.glUseProgram(this.programID);
		this.open = true;
	}

	public void stop() {
		GL20.glUseProgram(0);
		this.open = false;
	}

	protected void bindAttribute(int attribute, String variableName) {
		GL20.glBindAttribLocation(this.programID, attribute, variableName);
	}

	protected abstract void bindAttributes();

	public int getVertexShaderID() {
		return vertexShaderID;
	}

	public int getFragmentShaderID() {
		return fragmentShaderID;
	}
	
	public int getGeometryShaderID() {
		return geometryShaderID;
	}

	private static int loadShader(String file, int type) {
		StringBuilder shaderSource = new StringBuilder();
		try {
			BufferedReader reader = new FileReader(file).read();
			String line;
			while ((line = reader.readLine()) != null) {
				shaderSource.append(line).append("\n");
			}
			reader.close();
		} catch (Exception e) {
			System.err.println("Fichier inconnu");
			e.printStackTrace();
			System.exit(-1);
		}
		int shaderID = GL20.glCreateShader(type);
		GL20.glShaderSource(shaderID, shaderSource);
		GL20.glCompileShader(shaderID);
		if (GL20.glGetShaderi(shaderID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
			System.out.println(GL20.glGetShaderInfoLog(shaderID, 500));
			System.err.println("Erreur compilation shader dans le shader "+type);
			System.exit(-1);
		}
		return shaderID;
	}

	

}
