package fr.fcgame.client.utils.lwjgl.object;

import fr.fcgame.client.utils.lwjgl.model.Mesh;

public interface Meshable {

	public <M extends Mesh> M getMesh();
	
}
