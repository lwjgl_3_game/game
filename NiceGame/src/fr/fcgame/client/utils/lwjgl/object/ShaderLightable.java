package fr.fcgame.client.utils.lwjgl.object;

import fr.fcgame.client.utils.lwjgl.Light;

public interface ShaderLightable {

	public void loadLight(Light light);
	
}
