package fr.fcgame.client.utils.lwjgl.object;

/**
 * 
 * @author ComminQ_Q (Computer)
 *
 * @date 14/01/2020
 */
public interface Scalable {

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @return
	 * @date 14/01/2020
	 */
	public float scale();
	
	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param scale
	 * @date 14/01/2020
	 */
	public void scale(float scale);
	
}
