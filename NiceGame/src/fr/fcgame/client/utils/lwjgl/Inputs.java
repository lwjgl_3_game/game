package fr.fcgame.client.utils.lwjgl;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.glfwGetCursorPos;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetCursorPos;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;

import java.nio.DoubleBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWKeyCallbackI;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallbackI;

import fr.fcgame.client.Game;
import fr.fcgame.client.utils.java.Triplet;

public class Inputs {

	private static long window;
	private static final int KEYBOARD_SIZE = 512;
	private static final int MOUSE_SIZE = 16;

	private static int[] keyStates = new int[KEYBOARD_SIZE];
	private static boolean[] activeKeys = new boolean[KEYBOARD_SIZE];

	private static int[] mouseButtonStates = new int[MOUSE_SIZE];
	private static boolean[] activeMouseButtons = new boolean[MOUSE_SIZE];
	private static long lastMouseNS = 0;
	private static long mouseDoubleClickPeriodNS = 1000000000 / 5; // 5th of a second for double click.

	private static double oldX_Mouse;
	private static double oldY_Mouse;

	private static int NO_STATE = -1;

	protected static GLFWKeyCallback keyboard;

	protected static GLFWMouseButtonCallback mouse;

	public static void init(long window) {
		Inputs.window = window;

		glfwSetKeyCallback(window, new GLFWKeyCallbackI() {

			@Override
			public void invoke(long window, int key, int scancode, int action, int mods) {
				if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
					glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
				}
				if (key == -1) {
					return;
				}
				activeKeys[key] = action != GLFW_RELEASE;
				keyStates[key] = action;
			}
		});

		glfwSetMouseButtonCallback(window, new GLFWMouseButtonCallbackI() {

			@Override
			public void invoke(long window, int button, int action, int mods) {
				activeMouseButtons[button] = action != GLFW_RELEASE;
				mouseButtonStates[button] = action;
			}
		});
		resetKeyboard();
		resetMouse();
	}

	public static Triplet<Double, Double, Boolean> getMouseMovementDelta() {
		DoubleBuffer x = BufferUtils.createDoubleBuffer(1);
		DoubleBuffer y = BufferUtils.createDoubleBuffer(1);

		glfwGetCursorPos(window, x, y);
		x.rewind();
		y.rewind();

		double newX = x.get();
		double newY = y.get();

		double deltaX = newX - Game.WIDTH / 2;
		double deltaY = newY - Game.HEIGHT / 2;
		boolean rotX = newX != oldX_Mouse;
		boolean rotY = newY != oldY_Mouse;
		oldX_Mouse = newX;
		oldY_Mouse = newY;
		glfwSetCursorPos(window, Game.WIDTH / 2, Game.HEIGHT / 2);
		return new Triplet<Double, Double, Boolean>(deltaX, deltaY, rotX | rotY);
	}

	public static void update() {
		resetKeyboard();
		resetMouse();

		glfwPollEvents();
	}

	private static void resetKeyboard() {
		for (int i = 0; i < keyStates.length; i++) {
			keyStates[i] = NO_STATE;
		}
	}

	private static void resetMouse() {
		for (int i = 0; i < mouseButtonStates.length; i++) {
			mouseButtonStates[i] = NO_STATE;
		}

		long now = System.nanoTime();

		if (now - lastMouseNS > mouseDoubleClickPeriodNS)
			lastMouseNS = 0;
	}

	public static boolean keyDown(int key) {
		return activeKeys[key];
	}

	public static boolean keyPressed(int key) {
		return keyStates[key] == GLFW_PRESS;
	}

	public static boolean keyReleased(int key) {
		return keyStates[key] == GLFW_RELEASE;
	}

	public static boolean mouseButtonDown(int button) {
		return activeMouseButtons[button];
	}

	public static boolean mouseButtonPressed(int button) {
		return mouseButtonStates[button] == GLFW_RELEASE;
	}

	public static boolean mouseButtonReleased(int button) {
		boolean flag = mouseButtonStates[button] == GLFW_RELEASE;

		if (flag)
			lastMouseNS = System.nanoTime();

		return flag;
	}

	public static boolean mouseButtonDoubleClicked(int button) {
		long last = lastMouseNS;
		boolean flag = mouseButtonReleased(button);

		long now = System.nanoTime();

		if (flag && now - last < mouseDoubleClickPeriodNS) {
			lastMouseNS = 0;
			return true;
		}

		return false;
	}

}
