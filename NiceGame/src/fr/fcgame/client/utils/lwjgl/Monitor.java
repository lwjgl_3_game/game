package fr.fcgame.client.utils.lwjgl;

import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWVidMode;

public class Monitor {

	private long monitorID;
	
	public Monitor(long monitorID) {
		this.monitorID = monitorID;
	}
	
	protected GLFWVidMode getVideoMode() {
		return glfwGetVideoMode(this.monitorID);
	}
	
	public String getName() {
		return GLFW.glfwGetMonitorName(this.monitorID);
	}
	
	public int getWidth() {
		return getVideoMode().width();
	}
	
	public int getRefreshRate() {
		return getVideoMode().refreshRate();
	}
	
	public int getHeight() {
		return getVideoMode().height();
	}

	public long getMonitorID() {
		return monitorID;
	}
	
}
