package fr.fcgame.client.utils.ui;

import java.awt.Color;

import fr.fcgame.client.Game;
import fr.fcgame.client.utils.lwjgl.Window;
import fr.fcgame.client.utils.lwjgl.model.IndicesMesh;
import fr.fcgame.client.utils.lwjgl.model.MeshData;
import fr.fcgame.client.utils.ui.constraints.UiConstraintSet;

public abstract class UiComponent{

	private static MainComponent mainScreen;
	
	public static MainComponent getMainScreen() {
		return mainScreen;
	}
	
	public static void init(Window window, Game game) {
		if(mainScreen == null) {
			mainScreen = new MainComponent(window);
		}
	}
	
	private Color color;
	private UiComponent parent;
	
	// All pixels component
	public int topXpx;
	public int topYpx;
	public int widthXpx;
	public int heightYpx;
	public int zIndex;
	private IndicesMesh uiMesh;
	
	public UiComponent() {
		this(getMainScreen());
	}
	
	public UiComponent(UiComponent parent) {
		this.parent = parent;
		if(parent != null) this.zIndex = parent.zIndex + 1;
		this.color = new Color(0.5f, 0.5f, 0.5f);
	}
	
	public boolean isMainScreen() {
		return this.parent == null;
	}
	
 	public UiComponent getParent() {
		return parent;
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	public IndicesMesh getUiMesh() {
		return uiMesh;
	}
	
	public void setUiMesh(IndicesMesh uiMesh) {
		this.uiMesh = uiMesh;
	}
	
	public abstract MeshData buildMesh(MainComponent mainComponent, UiComponent parent);
	
	public void init(UiComponent parent, UiConstraintSet constraints) {
		this.widthXpx = constraints.widthConstraint.calcValueInPixel(this, parent, Direction.WIDTH);
		this.heightYpx = constraints.heightConstraint.calcValueInPixel(this, parent, Direction.HEIGHT);
		
		this.topXpx = constraints.xConstraint.calcValueInPixel(this, parent, Direction.TOPX);
		this.topYpx = constraints.yConstraint.calcValueInPixel(this, parent, Direction.TOPY);
	}
	
	protected abstract void update();

	public float getWidth() {
		return this.widthXpx;
	}
	
	public float getHeight() {
		return this.heightYpx;
	}
	
	
	
	
	
	
}
