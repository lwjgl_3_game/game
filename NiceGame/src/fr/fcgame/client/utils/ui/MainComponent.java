package fr.fcgame.client.utils.ui;

import java.awt.Color;
import java.util.Map;

import org.joml.Vector2f;

import fr.fcgame.client.utils.lwjgl.Window;
import fr.fcgame.client.utils.lwjgl.model.MeshData;
import fr.fcgame.client.utils.math.MathUtils;
import fr.fcgame.client.utils.ui.component.UiAggregation;
import fr.fcgame.client.utils.ui.constraints.UiConstraintSet;

/**
 * 
 * @author ComminQ_Q (Computer)
 *
 * @date 03/12/2019
 */
public class MainComponent extends UiAggregation{

	private static final float TOPX = -1;
	private static final float TOPY = 1;
	private static final float BOTX = 1;
	private static final float BOTY = -1;

	private Window window;
	
	public static void initComponent() {
		MainComponent.getMainScreen().initChilds();
	}
	
	public static void addComponent(UiComponent component, UiConstraintSet constraintSet) {
		MainComponent.getMainScreen().addChild(component, constraintSet);
	}
	
	public MainComponent(Window window) {
		super(null);
		this.window = window;
		this.setColor(new Color(0, 0, 0, 0));
		
		this.topXpx = 0;
		this.topYpx = 0;
		this.widthXpx = window.getWidth();
		this.heightYpx = window.getHeight();
		
		Vector2f ct = getCenterPixel();
		zIndex = -1;
	}
	
	public float pixelToGLValue(int px, boolean hor) {
		return MathUtils.map(px, 0, hor ? this.getWidth() : this.getHeight(), -1f, 1f);
	}
	
	public float getTopX() {
		return 0;
	}
	
	public float getTopY() {
		return 0;
	}
	
	public Vector2f getUnitPerPixel() {
		return new Vector2f(2 / getWidth(), 2 / getHeight());
	}
	
	public Vector2f getCenterPixel() {
		return new Vector2f(getWidth() / 2, getHeight() / 2);
	}
	
	public float getAspectRatio() {
		return this.window.getAspectRatio();
	}
	
	public float getWidth() {
		return this.window.getWidth();
	}
	
	public float getHeight() {
		return this.window.getHeight();
	}

	@Override
	protected void update() {
		// TODO Auto-generated method stub
		
	}	
}
