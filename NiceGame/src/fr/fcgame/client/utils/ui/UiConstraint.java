package fr.fcgame.client.utils.ui;

public abstract class UiConstraint {

	public abstract int calcValueInPixel(UiComponent current, UiComponent parent, Direction constraint);

}
