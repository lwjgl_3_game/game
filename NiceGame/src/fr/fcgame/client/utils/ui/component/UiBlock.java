package fr.fcgame.client.utils.ui.component;

import java.awt.Color;

import fr.fcgame.client.utils.lwjgl.model.MeshData;
import fr.fcgame.client.utils.ui.Direction;
import fr.fcgame.client.utils.ui.MainComponent;
import fr.fcgame.client.utils.ui.UiComponent;
import fr.fcgame.client.utils.ui.constraints.UiConstraintSet;
import fr.fcgame.client.utils.ui.def.Backgroundable;
import fr.fcgame.client.utils.ui.def.RoundBorder;

public class UiBlock extends UiComponent implements RoundBorder, Backgroundable{
	
	private Color backgroundColor;
	private int borderRadius;
	
	public UiBlock(UiComponent parent, Color background) {
		this(parent, background, 0);
	}
	
	public UiBlock(UiComponent parent, Color background, int borderRadius) {
		super(parent);
		this.backgroundColor = background;
		this.borderRadius = borderRadius;
	}
	
	@Override
	public Color getBackgroundColor() {
		return this.backgroundColor;
	}

	@Override
	public int getBorderRadius() {
		return borderRadius;
	}

	@Override
	protected void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public MeshData buildMesh(MainComponent main, UiComponent parent) {
		float positions[] = {
				this.topXpx, 				this.topXpx-this.heightYpx, 0, // 0
				this.topXpx+this.widthXpx, 	this.topXpx-this.heightYpx, 0,// 1
				this.topXpx,  				this.heightYpx, 0,// 2
				this.topXpx+this.widthXpx,  this.heightYpx, 0// 3
		 };
		 int indices[] = {
				 0, 1, 2,
				 1, 2, 3
		 };
		MeshData data = new MeshData();
		data.addIntArray(MeshData.INDICES, indices);
		data.addFloatArray(MeshData.POSITIONS, positions);
		return data;
	}
	

}
