package fr.fcgame.client.utils.ui.component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.fcgame.client.utils.java.wrappers.IntegerWrapper;
import fr.fcgame.client.utils.lwjgl.model.MeshData;
import fr.fcgame.client.utils.ui.MainComponent;
import fr.fcgame.client.utils.ui.UiComponent;
import fr.fcgame.client.utils.ui.constraints.UiConstraintSet;

public abstract class UiAggregation extends UiComponent{


	private Map<UiComponent, UiConstraintSet> childs;
	
	public UiAggregation() {
		super();
		this.childs = new HashMap<>();
	}

	public UiAggregation(UiComponent parent) {
		super(parent);
		this.childs = new HashMap<>();
	}

	public void initChilds() {
		for(UiComponent component : this.getChilds().keySet()) {
			component.init(this, this.getChilds().get(component));
		}
	}
	
	public void addChild(UiComponent uiComponent, UiConstraintSet set) {
		this.childs.put(uiComponent, set);
	}
	
	public Map<UiComponent, UiConstraintSet> getChilds() {
		return childs;
	}
	
	@Override
	public MeshData buildMesh(MainComponent mainComponent, UiComponent parent) {
		MeshData meshData = new MeshData();
		List<Integer> indices = new ArrayList<>();
		List<Float> positions = new ArrayList<>();
		IntegerWrapper indicesCount = new IntegerWrapper(0);
		for(UiComponent uiComponent : this.childs.keySet()) {
			uiComponent.init(this, this.childs.get(uiComponent));
			
			MeshData currentMeshData = uiComponent.buildMesh(mainComponent, this);
			float[] currentPositions = currentMeshData.getFloatArray(MeshData.POSITIONS);
			addMeshFace(indices, positions, indicesCount, currentPositions);
		}
		float[] positionsArray = new float[positions.size()];
		int[] indicesArray = new int[indices.size()];

		for (int i = 0; i < indices.size(); i++) indicesArray[i] = indices.get(i);
		for (int i = 0; i < positions.size(); i++) positionsArray[i] = positions.get(i);
		
		meshData.addIntArray(MeshData.INDICES, indicesArray);
		meshData.addFloatArray(MeshData.POSITIONS, positionsArray);
		
		return meshData;
	}
	
	protected void addMeshFace(List<Integer> indices, List<Float> positions, IntegerWrapper indicesCount, float[] allPositions) {
		int dim3Length = allPositions.length / 3;
		int faceLength = 3*4;
		int faceCount = dim3Length/4;
	
		float[] vertices = new float[3];
		
		for(int face = 0; face < faceCount; face++) {
			for(int i = 0; i < 4; i++) {
				System.arraycopy(allPositions, (face*faceLength)+(i*3), vertices, 0, 3);
				for(float f : vertices) {
					positions.add(f);
				}
			}
			indices.add(indicesCount.getValue());
			indices.add(indicesCount.getValue()+1);
			indices.add(indicesCount.getValue()+2);
			indices.add(indicesCount.getValue()+2);
			indices.add(indicesCount.getValue()+3);
			indices.add(indicesCount.getValue());
			
			indicesCount.add(4);
			
			
		}
	}
}
