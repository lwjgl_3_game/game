package fr.fcgame.client.utils.ui;

public abstract class InteractableUi extends UiComponent{

	protected abstract void mouseOver(float relX, float relY);
	
	protected abstract void mouseClick(float relX, float relY);
	
}
