package fr.fcgame.client.utils.ui.def;

public interface RoundBorder {

	int getBorderRadius();
	
}
