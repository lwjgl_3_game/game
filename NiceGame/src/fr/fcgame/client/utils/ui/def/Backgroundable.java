package fr.fcgame.client.utils.ui.def;

import java.awt.Color;

public interface Backgroundable {

	Color getBackgroundColor();
	
}
