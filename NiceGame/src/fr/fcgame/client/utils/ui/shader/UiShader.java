package fr.fcgame.client.utils.ui.shader;

import java.awt.Color;

import org.joml.Matrix4f;

import fr.fcgame.client.utils.Utils;
import fr.fcgame.client.utils.lwjgl.Window;
import fr.fcgame.client.utils.lwjgl.shaders.ShaderProgram;
import fr.fcgame.client.utils.math.MathUtils;

public class UiShader extends ShaderProgram{

	public static final String UI_SHADER_LOCATION = "resources/shaders/ui/";
	
	private Matrix4f projectionMatrix;
	
	public UiShader() {
		super(UI_SHADER_LOCATION+"uiVertexShader.glsl", UI_SHADER_LOCATION+"uiFragmentShader.glsl");
	
	}
	
	public void buildProjectionMatrix() {
		this.projectionMatrix = MathUtils.createOrthoProjection(Window.getWindow());
		start();
		loadOrthoMatrix(this.projectionMatrix);
		stop();
	}

	@Override
	protected void getAllUniformLocations() {
		registerUniform("color");
		registerUniform("projectionMatrix");
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
	}
	
	public void loadColor(Color color) {
		super.injectVector4f(getUniformLocation("color"), Utils.rgbaToVec4f(color));
	}
	
	public void loadOrthoMatrix(Matrix4f matrix) {
		super.injectMatrix(getUniformLocation("projectionMatrix"), matrix);
	}

}
