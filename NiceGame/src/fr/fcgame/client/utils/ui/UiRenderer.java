package fr.fcgame.client.utils.ui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import fr.fcgame.client.Game;
import fr.fcgame.client.utils.lwjgl.Renderer;
import fr.fcgame.client.utils.lwjgl.Window;
import fr.fcgame.client.utils.lwjgl.model.IndicesMesh;
import fr.fcgame.client.utils.lwjgl.model.MeshData;
import fr.fcgame.client.utils.lwjgl.shaders.ShaderProgram;
import fr.fcgame.client.utils.ui.component.UiAggregation;
import fr.fcgame.client.utils.ui.component.UiBlock;
import fr.fcgame.client.utils.ui.constraints.AspectRatioConstraint;
import fr.fcgame.client.utils.ui.constraints.CenterConstraint;
import fr.fcgame.client.utils.ui.constraints.PixelConstraint;
import fr.fcgame.client.utils.ui.constraints.UiConstraintSet;
import fr.fcgame.client.utils.ui.def.Backgroundable;
import fr.fcgame.client.utils.ui.shader.UiShader;
import fr.fcgame.client.utils.vao.Vao;

public class UiRenderer extends Renderer<UiComponent> {

	private List<UiAggregation> components;

	public UiRenderer() {
		this.setLinkedShader(UiShader.class);
		this.components = new ArrayList<>();
		this.components.add(MainComponent.getMainScreen());
	}
	
	public void buildVao(UiComponent parent, MeshData data, IndicesMesh mesh) {
		int[] indices = data.getIntArray(MeshData.INDICES);
		float[] positions = data.getFloatArray(MeshData.POSITIONS);
		for(int i = 0; i < positions.length/3; i++) {
			positions[i*3] = positions[i*3] - parent.getWidth()/2.0f;
			positions[(i*3)+1] = positions[(i*3)+1] + parent.getHeight()/2.0f;
		}
		Vao vao = Vao.createVao();
		vao.bind();
		vao.bindIndices(indices);
		vao.bindData(positions, 3);
		vao.unbind();
		mesh.setVao(vao);
		mesh.setIndicesLength(indices.length);
	}
	
	public void buildUiComponent(UiComponent component, UiComponent parent) {
		IndicesMesh mesh = new IndicesMesh(null, -1);
		MeshData data = component.buildMesh(MainComponent.getMainScreen(), parent);
		buildVao(parent, data, mesh);
		component.setUiMesh(mesh);
	}

	@Override
	protected void render(Game game, Window window, ShaderProgram shaderProgram) {
		injectMainUniforms(shaderProgram);
		for (UiComponent uiComponent : this.components) {

			Vao vao = uiComponent.getUiMesh().getVao();
			
			vao.bind();
			vao.enableVBOs();
			injectForEachUniforms(uiComponent, shaderProgram);
			uiComponent.getUiMesh().drawMesh();
			vao.disableVBOs();
			vao.unbind();

		}

	}

	@Override
	public void init(Game game, Window window) {
		UiBlock uiBlock = new UiBlock(MainComponent.getMainScreen(), new Color(0, 255, 0, 255));
		UiConstraintSet constraints = new UiConstraintSet();
		constraints.setxConstraint(new CenterConstraint());
		constraints.setyConstraint(new CenterConstraint());
		constraints.setWidthConstraint(new PixelConstraint(10));
		constraints.setHeightConstraint(new AspectRatioConstraint(1));
		
		MainComponent.addComponent(uiBlock, constraints);
		MainComponent.initComponent();
		MainComponent main = MainComponent.getMainScreen();
		
		buildUiComponent(main, main);
	}

	@Override
	protected void injectMainUniforms(ShaderProgram shaderProgram) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void injectForEachUniforms(UiComponent type, ShaderProgram shaderProgram) {
		UiShader uiShader = Game.getGame().getMainRenderer().getMyShader(this);
		if(type instanceof Backgroundable) {
			uiShader.loadColor(((Backgroundable)type).getBackgroundColor());
		}else {
			uiShader.loadColor(type.getColor());
		}
	}

}
