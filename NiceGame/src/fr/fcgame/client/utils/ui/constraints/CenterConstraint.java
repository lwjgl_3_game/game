package fr.fcgame.client.utils.ui.constraints;

import fr.fcgame.client.utils.ui.Direction;
import fr.fcgame.client.utils.ui.UiComponent;
import fr.fcgame.client.utils.ui.UiConstraint;

public class CenterConstraint extends UiConstraint {

	@Override
	public int calcValueInPixel(UiComponent current, UiComponent parent, Direction constraint) {
		switch (constraint) {
		case HEIGHT:
			return current.heightYpx;
		case WIDTH:
			return current.widthXpx;
		case TOPX:
			
			int topParentX = parent.topXpx;
			int widthParent = parent.widthXpx;
			int width = current.widthXpx;
			int centerParentX = (topParentX + widthParent) / 2;
			int width2 = width/2;
			
			int topX = (centerParentX - width2);
			return topX;
		case TOPY:
			int topParentY = parent.topXpx;
			int heightParent = parent.widthXpx;
			int height = current.widthXpx;
			int centerParentY = (topParentY + heightParent) / 2;
			int height2 = height/2;
			
			int topY = (centerParentY - height2);
			return topY;
		}
		throw new IllegalArgumentException("La Direction est nulle");
	}

}
