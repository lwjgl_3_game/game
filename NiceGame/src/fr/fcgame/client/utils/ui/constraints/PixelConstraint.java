package fr.fcgame.client.utils.ui.constraints;

import fr.fcgame.client.utils.ui.Direction;
import fr.fcgame.client.utils.ui.UiComponent;
import fr.fcgame.client.utils.ui.UiConstraint;

public class PixelConstraint extends UiConstraint{

	private int value;
		
	public PixelConstraint(int value) {
		super();
		this.value = value;
	}

	@Override
	public int calcValueInPixel(UiComponent current, UiComponent parent, Direction constraint) {
		return this.value;
	}

}
