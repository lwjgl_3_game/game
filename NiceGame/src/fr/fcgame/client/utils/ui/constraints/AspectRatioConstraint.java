package fr.fcgame.client.utils.ui.constraints;

import fr.fcgame.client.utils.ui.Direction;
import fr.fcgame.client.utils.ui.UiComponent;
import fr.fcgame.client.utils.ui.UiConstraint;

public class AspectRatioConstraint extends UiConstraint {

	private float ratio;

	public AspectRatioConstraint(float ratio) {
		super();
		this.ratio = ratio;
	}

	@Override
	public int calcValueInPixel(UiComponent current, UiComponent parent, Direction constraint) {
		switch (constraint) {
		case HEIGHT:
			int heightpx = (int) ((current.widthXpx * this.ratio));
			return heightpx;
		case TOPX:
			return current.topXpx;
		case TOPY:
			return current.topYpx;
		case WIDTH:
			return current.widthXpx;
		}
		throw new IllegalArgumentException("");
	}

}
