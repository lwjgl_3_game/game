package fr.fcgame.client.utils.ui.constraints;

import fr.fcgame.client.utils.ui.UiConstraint;

public class UiConstraintSet {

	public UiConstraint xConstraint;
	public UiConstraint yConstraint;
	public UiConstraint widthConstraint;
	public UiConstraint heightConstraint;
	
	public void setxConstraint(UiConstraint xConstraint) {
		this.xConstraint = xConstraint;
	}
	
	public void setyConstraint(UiConstraint yConstraint) {
		this.yConstraint = yConstraint;
	}
	
	public void setWidthConstraint(UiConstraint widthConstraint) {
		this.widthConstraint = widthConstraint;
	}
	
	public void setHeightConstraint(UiConstraint heightConstraint) {
		this.heightConstraint = heightConstraint;
	}
	
}
