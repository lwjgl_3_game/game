package fr.fcgame.client.main;

import org.lwjgl.glfw.GLFWErrorCallback;

import fr.fcgame.client.Game;

/**
 * Classe principale
 * @author ComminQ_Q (Computer)
 *
 * @date 21/12/2019
 */
public class Main {
	
	/**
	 * Classe Main
	 * @author ComminQ_Q (Computer)
	 *
	 * @param args
	 * @date 21/12/2019
	 */
	public static void main(String[] args) {
		GLFWErrorCallback.createPrint(System.err).set();
		Game.getGame().run();
	}
	
}
